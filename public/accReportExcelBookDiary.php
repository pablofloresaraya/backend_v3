<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

//require_once '/var/www/html/backend/public/PHPExcel-1.8/Classes/PHPExcel.php';

use Dotenv\Dotenv;
use App\Negocio\Token;
use App\Negocio\Contabilidad;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;

require __DIR__ . '/../vendor/autoload.php';
$php_input=json_decode(file_get_contents('php://input'),1);

$dotenv = Dotenv::createImmutable(__DIR__ . "/../");
$dotenv->load();

$loggerPath  = $_ENV["LOG"];
$servername  = $_ENV["SERVERNAME"];
$basepath    = $_ENV["BASEPATH"];
$secret      = $_ENV["JWT_SECRET"];
$algorithm   = $_ENV["JWT_ALGORITHM"];
$hrs_expired = $_ENV["JWT_HRS_EXPIRED"];
$username    = "admin";

$loggerSettings = array(
    'name' => 'klinik-backend',
    'path' => '/var/www/html/backend/logs/app.log',
    'level' => Logger::DEBUG,
);

$logger = new Logger($loggerSettings['name']);

$processor = new UidProcessor();
$logger->pushProcessor($processor);

$handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
$logger->pushHandler($handler);

$jwt_data["algorithm"]   = $algorithm;
$jwt_data["secretkey"]   = $secret;
$jwt_data["servername"]  = $servername;
$jwt_data["username"]    = $username;
$jwt_data["hrs_expired"] = $hrs_expired;

$p_fechaini  = (empty($_GET["i"]))? '':base64_decode($_GET["i"]);
$p_fechafin  = (empty($_GET["f"]))? '':base64_decode($_GET["f"]);
$p_periodo   = (empty($_GET["p"]))? '':base64_decode($_GET["p"]);
$p_cuentagen = (empty($_GET["g"]))? '':base64_decode($_GET["g"]);

$obj_token = new Token($jwt_data,$logger);

$param_c = (empty($_GET["c"]))? '':base64_decode($_GET["c"]); 

if(empty($_GET["t"])){
    $logger->error("token no existe");
    exit;    
}

$param_t = $_GET["t"];

$token_validate = $obj_token->validateToken($param_t);
$logger->info("Estado token: ",$token_validate);
if($token_validate["status"]=="ERROR"){
    header($token_validate["header"]);
    exit;
}

$logger->info("Iniciando clases de negocios");

$param1["company_id"] = $param_c;
$contabilidad = new Contabilidad($param1,$logger);

date_default_timezone_set("America/Santiago");
$hora = date("G:i");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

$objPHPExcel = new Spreadsheet();

$objPHPExcel->getProperties()
->setCreator("Klinik")
->setLastModifiedBy("Klinik")
->setTitle("Reporte Libro Diario")
->setSubject("Reporte Libro Diario")
->setDescription("Reporte Libro Diario")
->setKeywords("Template excel");

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:F2');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B3:F3');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B4:F4');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:F5');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:G6');

$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray(
    array   ( 'font' => array( 'bold' => true) )
);
$objPHPExcel->getActiveSheet()->getStyle('A9:G9')->applyFromArray(
    array   ( 'font' => array( 'bold' => true) )
);
$objPHPExcel->getActiveSheet()->getStyle('A6:G6')->applyFromArray(
    array   ( 'font' => array( 'bold' => true) )
);

$partner = $contabilidad->accGetDataPartner($p_cuentagen);

$cabecera = array();

if(empty($p_periodo)){ //reporte por fechas

    $cabecera = $contabilidad->accReportDiaryBookDate($p_fechaini,$p_fechafin);

}else{ //reporte por periodo

    $cabecera = $contabilidad->accReportDiaryBookPeriod($p_periodo);

}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->SetCellValue('A1', $partner['data'][0]['nombre']);
$objPHPExcel->getActiveSheet()->SetCellValue('G1', "Página 1");
$objPHPExcel->getActiveSheet()->SetCellValue('G2', "Hora: ".$hora);
$objPHPExcel->getActiveSheet()->SetCellValue('A2', "R.U.T :");
$objPHPExcel->getActiveSheet()->SetCellValue('B2', $partner['data'][0]['rut']);
$objPHPExcel->getActiveSheet()->SetCellValue('A3', "GIRO :");
$objPHPExcel->getActiveSheet()->SetCellValue('B3', $partner['data'][0]['giro']);
$objPHPExcel->getActiveSheet()->SetCellValue('A4', "DIRECCIÓN :");
$objPHPExcel->getActiveSheet()->SetCellValue('B4', $partner['data'][0]['direccion']);
$objPHPExcel->getActiveSheet()->SetCellValue('A5', "CIUDAD :");
$objPHPExcel->getActiveSheet()->SetCellValue('B5', $partner['data'][0]['ciudad']);
$objPHPExcel->getActiveSheet()->SetCellValue('A6', "LIBRO DIARIO");

$objPHPExcel->getActiveSheet()->getStyle('A6:G6')->getAlignment()->setHorizontal('center');

//cell size automatic
foreach(range('A2','A5') as $columnID){
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

$objPHPExcel->getActiveSheet()->SetCellValue('A9', "Cta.");
$objPHPExcel->getActiveSheet()->SetCellValue('B9', "Nombre");
$objPHPExcel->getActiveSheet()->SetCellValue('C9', "Tipo");
$objPHPExcel->getActiveSheet()->SetCellValue('D9', "Número");
$objPHPExcel->getActiveSheet()->SetCellValue('E9', "Glosa");
$objPHPExcel->getActiveSheet()->SetCellValue('F9', "DEBITO");
$objPHPExcel->getActiveSheet()->SetCellValue('G9', "CREDITO");

/*$border_style= array('borders' => array('top' => array('style' => 
PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => 'FF000000'),)));*/

$total_debito  = 0;
$total_credito = 0;
$j=9;

for ($i=0;$i<count($cabecera['data']);$i++){

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A'.($j+1), "")
    ->setCellValue('B'.($j+1), "Comprobante: ".$cabecera['data'][$i]['comprobante']." Tipo:" )
    ->setCellValue('C'.($j+1), $cabecera['data'][$i]['tipo'])
    ->setCellValue('D'.($j+1), "Fecha: ".$cabecera['data'][$i]['fecha'])
    ->setCellValue('E'.($j+1), "")
    ->setCellValue('F'.($j+1), "")
    ->setCellValue('G'.($j+1), "");

    $detalles = $contabilidad->accReportDiaryBookDetail($cabecera['data'][$i]['id']);
    
    $z=($j+1);
    $total_comprobante_debito  = 0;
    $total_comprobante_credito = 0;
    
    for($n=0; $n<count($detalles['data']); $n++){

        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.($z+1), $detalles['data'][$n]['nro_cuenta'] )
        ->setCellValue('B'.($z+1), $detalles['data'][$n]['nombre_cuenta'] )
        ->setCellValue('C'.($z+1), "")
        ->setCellValue('D'.($z+1), "")
        ->setCellValue('E'.($z+1), $detalles['data'][$n]['glosa'] )
        ->setCellValue('F'.($z+1), $detalles['data'][$n]['debito'] )
        ->setCellValue('G'.($z+1), $detalles['data'][$n]['credito'] );

        $total_comprobante_debito  += $detalles['data'][$n]['debito'];
        $total_comprobante_credito += $detalles['data'][$n]['credito'];
        $z++;

    }

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A'.($z+1), "" )
    ->setCellValue('B'.($z+1), "" )
    ->setCellValue('C'.($z+1), "")
    ->setCellValue('D'.($z+1), "")
    ->setCellValue('E'.($z+1), "TOTAL COMPROBANTE" )
    ->setCellValue('F'.($z+1), $total_comprobante_debito )
    ->setCellValue('G'.($z+1), $total_comprobante_credito );
    

    $total_debito  += $total_comprobante_debito;
    $total_credito += $total_comprobante_credito;
    $j=($z+1);

}

$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A'.($j+1), "" )
->setCellValue('B'.($j+1), "" )
->setCellValue('C'.($j+1), "")
->setCellValue('D'.($j+1), "")
->setCellValue('E'.($j+1), "" )
->setCellValue('F'.($j+1), $total_debito )
->setCellValue('G'.($j+1), $total_credito );


foreach(range('B10','B'.$j) as $columnID){
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

foreach(range('E10','E'.$j) as $columnID){
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

foreach(range('F10','F'.$j) as $columnID){
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

foreach(range('G10','G'.$j) as $columnID){
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}

$writer = IOFactory::createWriter($objPHPExcel, "Xlsx"); //Xls is also possible 
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="excel.xls"');
header('Cache-Control: max-age=0');
$writer->save('php://output');
