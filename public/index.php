<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

//use Firebase\JWT\JWT;
use Dotenv\Dotenv;
//use Tuupola\Base62;
use App\Negocio\Token;
use App\Negocio\Contabilidad;
use App\Negocio\Engine;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;

require __DIR__ . '/../vendor/autoload.php';
$php_input=json_decode(file_get_contents('php://input'),1);
//$data["php_post"]=$_POST;
//$data["php_files"]=$_FILES;

$dotenv = Dotenv::createImmutable(__DIR__ . "/../");
$dotenv->load();

$loggerPath = $_ENV["LOG"];
$servername = $_ENV["SERVERNAME"];
$basepath = $_ENV["BASEPATH"];
$secret = $_ENV["JWT_SECRET"];
$algorithm = $_ENV["JWT_ALGORITHM"];
$hrs_expired = $_ENV["JWT_HRS_EXPIRED"];
$username = "admin";

$loggerSettings = array(
    'name' => 'klinik-backend',
    'path' => '/var/www/html/klinik-software/backend/logs/app.log',
    'level' => Logger::DEBUG,
);

$logger = new Logger($loggerSettings['name']);

$processor = new UidProcessor();
$logger->pushProcessor($processor);

$handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
$logger->pushHandler($handler);

$jwt_data["algorithm"] = $algorithm;
$jwt_data["secretkey"] = $secret;
$jwt_data["servername"] = $servername;
$jwt_data["username"] = $username;
$jwt_data["hrs_expired"] = $hrs_expired;

$obj_token = new Token($jwt_data,$logger);

//if(!empty($_GET)){
//    $param_r = (empty($_GET["r"]))? '':base64_decode($_GET["r"]);
//    $param_o = (empty($_GET["o"]))? '':base64_decode($_GET["o"]);
//    $param_t = (empty($_GET["o"]))? '':base64_decode($_GET["t"]);
//}
//else if(!empty($_POST)){
//    $param_r = (empty($_POST["r"]))? '':base64_decode($_POST["r"]);
//    $param_o = (empty($_POST["o"]))? '':base64_decode($_POST["o"]);  
//    $param_t = (empty($_POST["o"]))? '':base64_decode($_POST["t"]);  
//}
$param_r = (empty($_GET["r"]))? '':base64_decode($_GET["r"]);
$param_o = (empty($_GET["o"]))? '':base64_decode($_GET["o"]);  
$param_c = (empty($_GET["c"]))? '':base64_decode($_GET["c"]);  

$esLogin = ($param_r=="login")? true:false;
if($esLogin){
    $engine = new Engine(array(),$logger);
    //Validar usuario y contraseña. Si pasa validación crear token
    if(!empty($_POST["username"]) && !empty($_POST["password"])){
        $p_username = $_POST["username"];
        $p_password = $_POST["password"];

        $loginResponse = $engine->baseUserLogin($p_username,$p_password);
        if($loginResponse["status"]=='ok'){
            $token = $obj_token->encode();
            $token["username"] = $loginResponse["data"]["user"];
            $token["name"] = $loginResponse["data"]["name"];
            $token["company_list"] = $loginResponse["data"]["company_list"];
            $token["company_default"] = $loginResponse["data"]["company_default"];
            $token["company_used"] = $loginResponse["data"]["company_used"];
            
            echo json_encode($token);
        }
        else{
            header("HTTP/1.1 401 Unauthorized");
        }
    }
    else{
        header("HTTP/1.1 401 Unauthorized");
    }
}
else{
    if(empty($_GET["t"])){
        $logger->error("token no existe");
        exit;    
    }

    $param_t = $_GET["t"];
    //$logger->info("php_input: ".$php_input["o"],$php_input);
    //$logger->info("php_get: ",$_GET);
    //$logger->info("php_post: ",$_POST);
    //$logger->info("Cabecera: ",getallheaders());
    //$logger->info("Cabecera: ",apache_request_headers());
    //$header_data = getallheaders(); jelgueda: comentado hasta que sepa por qué se pierde el header Authorization cuando llamo desde fetch. Desde postman funciona bien
    //$token_validate = $obj_token->validate($header_data);
    $token_validate = $obj_token->validateToken($param_t);
    $logger->info("Estado token: ",$token_validate);
    if($token_validate["status"]=="ERROR"){
        header($token_validate["header"]);
        exit;
    }
    
    $logger->info("Iniciando clases de negocios");

    $param1["company_id"] = $param_c;
    $contabilidad = new Contabilidad($param1,$logger);
    $param2["company_id"] = $param_c;
    $engine = new Engine($param2,$logger);

    $logger->info("route: ".$param_r." ".$param_o);
    switch($param_r){
        case "vouchertypes":
            switch($param_o){
                case "1":
                    include_once $basepath."/src/Presentacion/accVoucherTypeListAll.php";
                    break;
            }
            break;
        case "module":
            switch($param_o){
                case "1":
                    include_once $basepath."/src/Presentacion/engGetModuleAll.php";
                    break;
            }
            break;
        case "accounts":
            switch($param_o){
                case "1":
                    include_once $basepath."/src/Presentacion/accAccountListTree.php";
                    break;
                case "2":
                    include_once $basepath."/src/Presentacion/accAccountListChargeable.php";
                    break;
            }
            break;
        case "accounting":
            switch($param_o){
                case "7":
                    include_once $basepath."/src/Presentacion/accAccountingReportBookDiaryPath.php";                    
                    break;
                case "8":
                    include_once $basepath."/src/Presentacion/accAccountingReportBookBigPath.php";                    
                    break;   
                case "101":
                    include_once $basepath."/src/Presentacion/accAccountingEntryImportDetail.php";
                    break;
                case "102":
                    include_once $basepath."/src/Presentacion/accAccountingEntrySave.php";
                    break;
                case "103":
                    include_once $basepath."/src/Presentacion/accAccountingPeriodIsOpen.php";
                    break;
                case "104":
                    include_once $basepath."/src/Presentacion/accAccountingEntryGet.php";
                    break;
                case "105":
                    include_once $basepath."/src/Presentacion/accAccountingEntryDelete.php";
                    break;
                case "106":
                    include_once $basepath."/src/Presentacion/accAccountingEntryPrint.php";
                    break;                
                case "107":
                    include_once $basepath."/src/Presentacion/accAccountingPagaresNoContabilizados.php";
                    break;
                case "108":
                    include_once $basepath."/src/Presentacion/accAccountingPagaresContabilizar.php";
                    break;               
                case "109":
                    include_once $basepath."/src/Presentacion/accAccountingConveniosNoContabilizados.php";
                    break;
                case "110":
                    include_once $basepath."/src/Presentacion/accAccountingConveniosContabilizar.php";
                    break;  
            }
            break;
        case "partners":
            switch($param_o){
                case "2":
                    include_once $basepath."/src/Presentacion/rhPartnerListCustomersSuppliers.php";
                    break;
                case "4":
                    include_once $basepath."/src/Presentacion/rhPartnerViewForRut.php";
                    break;
                case "5":
                    include_once $basepath."/src/Presentacion/rhPartnerSave.php";
                    break;
                case "6":
                    include_once $basepath."/src/Presentacion/rhPartnerCustomerSupplierDelete.php";
                    break;
                case "7":
                    include_once $basepath."/src/Presentacion/rhPartnerListSupplier.php";
                    break;
                case "101":
                    include_once $basepath."/src/Presentacion/rhPartnerSelectForIdentifier.php";
                    break;
            }
            break;
        case "territorial":
            switch($param_o){
                case "1":
                    include_once $basepath."/src/Presentacion/engTerritorialRegionsList.php";
                    break;
                case "2":
                    include_once $basepath."/src/Presentacion/engTerritorialProvincesList.php";
                    break;
                case "3":
                    include_once $basepath."/src/Presentacion/engTerritorialCommunesList.php";
                    break;
            }
            break;
        case "currency":
            switch($param_o){
                case "1":
                    include_once $basepath."/src/Presentacion/engBaseCurrencyView.php";
                    break;
                case "2":
                    include_once $basepath."/src/Presentacion/engBaseCurrencySave.php";
                    break;
                case "3":
                    include_once $basepath."/src/Presentacion/engBaseCurrencyDelete.php";
                    break;
                case "4":
                    include_once $basepath."/src/Presentacion/engBaseValueChangeView.php";
                    break;
                case "5":
                    include_once $basepath."/src/Presentacion/engBaseValueChangeDelete.php";
                    break;
                case "6":
                    include_once $basepath."/src/Presentacion/engBaseValueChangeSave.php";
                    break;
            }
            break;
        case "obligations":
            switch($param_o){
                case "1":
                    include_once $basepath."/src/Presentacion/engBaseDocumentType.php";
                    break;
                case "2":
                    include_once $basepath."/src/Presentacion/engBaseCostCenter.php";
                    break;
                case "3":
                    include_once $basepath."/src/Presentacion/engApsRegisterObligations.php";
                    break;
                case "4":
                    include_once $basepath."/src/Presentacion/engGetObligation.php";
                    break;
                case "5":
                    include_once $basepath."/src/Presentacion/engAnulationObligation.php";
                    break;

            }
            break;
    }   
}

?>