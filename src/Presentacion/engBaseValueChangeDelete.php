<?php

try{
    $logger->info("engBaseValueChangeDelete init");
    $data = $engine->engBaseValueChangeDelete($php_input);
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engBaseValueChangeDelete: ".$data);
}
header('Content-Type: application/json');
echo json_encode($data);