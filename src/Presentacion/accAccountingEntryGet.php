<?php
try{
    $logger->info("accAccountingEntryGet init");
    $data = $contabilidad->accEntryGet($_POST);
}
catch(Exception $e) {
    $data["status"] = 'error';
    $data["errores"][] = $e->getMessage();
    $data["message"] = "";
    $data["data"] = array();
    $logger->error("accAccountingEntryGet: ",$data);
}
header('Content-Type: application/json');
echo json_encode($data);
?>