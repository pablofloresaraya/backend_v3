<?php
try{
    $logger->info("engGetModuleAll init");
    $p_module_id = (empty($_POST["var1"]))? -1:(int) $_POST["var1"];
    $data = $engine->getModuleAll($p_module_id);
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engGetModuleAll: ".$data);
}
header('Content-Type: application/json');
echo json_encode($data);
?>