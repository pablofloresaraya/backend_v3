<?php
    $logger->info("accAccountingEntryImportDetail init");
    $data = $contabilidad->accEntryImportDetail($_FILES);
    header('Content-Type: application/json');
    echo json_encode($data);
/*
use PhpOffice\PhpSpreadsheet\IOFactory;

$file = $_FILES["fileEntryDetail"];
$name = $file["name"];
$rutaArchivo = $file["tmp_name"];
$info = new SplFileInfo($name);
$extension = $info->getExtension();

$correcto = in_array($extension,array("xls","xlsx"));
if($correcto){
    $data = array("status" => "", "message" => "", "data" => array());
    $documento = IOFactory::load($rutaArchivo);
    $totalDeHojas = $documento->getSheetCount();
    $hojaActual = $documento->getActiveSheet();
    $highestRow = $hojaActual->getHighestRow();
    $highestColumn = $hojaActual->getHighestColumn();

    //$lines = $highestRow - 1;
    //if ($lines <= 0) {
    //    $this->addError('Excel', 'No hay datos en la tabla de Excel');
    //    return false;
    //}
    for ($row = 1; $row <= $highestRow; ++$row) {
        $cuenta = $hojaActual->getCellByColumnAndRow(1, $row)->getValue();
        $debe = $hojaActual->getCellByColumnAndRow(2, $row)->getValue();
        $haber = $hojaActual->getCellByColumnAndRow(3, $row)->getValue();
        $rut = $hojaActual->getCellByColumnAndRow(4, $row)->getValue();
        $detalle = $hojaActual->getCellByColumnAndRow(5, $row)->getValue();

        $data["data"][$row-1] = array("line" => $row
                            , "id" => 0, "code" => $cuenta, "name" => ""
                            , "debit" => $debe, "credit" => $haber
                            , "rut" => $rut, "detalle" => $detalle
                        );
    }
}
//header("HTTP/1.1 ".$response["HTML_STATUS"]);
header('Content-Type: application/json');
echo json_encode($data);
*/

?>