<?php

header('Content-Type: application/json');

try{
    $logger->info("engBaseCurrencySave init");
    $data = $engine->engBaseCurrencySave($php_input);
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engBaseCurrencySave: ".$data);
}

echo json_encode($data);