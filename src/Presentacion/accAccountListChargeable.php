<?php

try{
    $logger->info("accAccountsListChargeable init");    
    $accounts = $contabilidad->accAccountsListChargeable();
    $data = $accounts;
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("accAccountsListChargeable: ".$data);
}

header('Content-Type: application/json');
echo json_encode($data);