<?php

header('Content-Type: application/json');

try{

    $logger->info("baseTerritorialCommunesForProvince init");
    $postData = file_get_contents("php://input");
    $post = json_decode($postData);

    $p_provincia = (empty($post->provincia)) ? "" : (int) $post->provincia;

    $communes = $engine->baseTerritorialCommunesForProvince($p_provincia);
    $data = $communes;

}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("baseTerritorialCommunesForProvince: ", $data);
}

echo json_encode($data);
?>