<?php
try{
    $logger->info("accAccountingPagaresNoContabilizados init");
    $data = $contabilidad->accPagaresNoContabilizados($_POST);
}
catch(Exception $e) {
    $data["header"] = 'error';
    $data["status"] = 'error';
    $data["errores"] = array($e->getMessage());
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("accAccountingPagaresNoContabilizados: ", $data);
}
header('Content-Type: application/json');
echo json_encode($data);
?>