<?php

header('Content-Type: application/json');

try{
    $logger->info("engAnulationObligation init");
    $data = $engine->engAnulationObligation($php_input);
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engAnulationObligation: ".$data);
}

echo json_encode($data);