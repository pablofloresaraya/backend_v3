<?php

try{
    $logger->info("engBaseDocumentType init");
    $data = $engine->engBaseDocumentType();
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engBaseDocumentType: ".$data);
}
header('Content-Type: application/json');
echo json_encode($data);