<?php
try{
    $logger->info("accAccountingPeriodIsOpen init");
    $p_date = (empty($_POST["var1"]))? "":(string) $_POST["var1"];
    $data = $contabilidad->accPeriodIsOpen($p_date);
}
catch(Exception $e) {
    $data["status"] = 'error';
    $data["errores"][] = $e->getMessage();
    $data["message"] = "";
    $data["data"] = array();
    $logger->error("accAccountingPeriodIsOpen: ".$data);
}
header('Content-Type: application/json');
echo json_encode(array("isopen"=>$data));
?>