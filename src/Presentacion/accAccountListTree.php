<?php
try{
    $logger->info("accAccountListTree init");
    $p_nombre = (empty($_POST["var1"]))? "":(string) $_POST["var1"];
    $data = $contabilidad->accAccountListTree($p_nombre);
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array($e->getMessage());
    $logger->error("accAccountListTree: ",$data);
}
header('Content-Type: application/json');
echo json_encode($data);
?>