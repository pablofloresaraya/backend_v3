<?php

try{
    $logger->info("engBaseCostCenter init");
    $data = $engine->engBaseCostCenter();
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engBaseCostCenter: ".$data);
}
header('Content-Type: application/json');
echo json_encode($data);