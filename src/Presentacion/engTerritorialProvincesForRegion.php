<?php

header('Content-Type: application/json');

try{

    $logger->info("baseTerritorialProvincesForRegion init");
    $postData = file_get_contents("php://input");
    $post = json_decode($postData);
    
    //id-region
    $p_region = (empty($post->region)) ? "" : (int) $post->region;

    $provinces  = $engine->baseTerritorialProvincesForRegion($p_region);
    $data = $provinces;
 
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("baseTerritorialProvincesForRegion: ", $data);
}

echo json_encode($data);

?>