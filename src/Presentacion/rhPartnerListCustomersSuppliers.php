<?php

try{
    $logger->info("rhPartnerListCustomersSuppliers init");    
    $ruts = $engine->rhPartnerListCustormesSuppliers();
    $data = $ruts;
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("rhPartnerListCustomersSuppliers: ", $data);
}

header('Content-Type: application/json');
echo json_encode($data);
