<?php

header('Content-Type: application/json');

try{
    $postData = file_get_contents("php://input");
    $post = json_decode($postData);
    $logger->info("rhPartnerViewForRut init");
    $run = (empty($post->run)) ? "" : (string) $post->run;
    $array = $engine->rhPartnerViewForRut($run);
    
    $data['id']         = $array['data'][0]['id'];
    $data['rut']        = $array['data'][0]['identifier'];
    $data['dig']        = $array['data'][0]['identifier_dv'];
    $data['cliente']    = $array['data'][0]['customer'];
    $data['proveedor']  = $array['data'][0]['supplier'];
    $data['nombre']     = $array['data'][0]['name'];
    $data['giro']       = !empty($array['data'][0]["company_activity"]) ? $array['data'][0]["company_activity"] : "";
    $data['direccion']  = $array['data'][0]['address'];
    $data['region']     = ['id' => $array['data'][0]['id_region'], 'name' => $array['data'][0]['name_region']];
    $data['provincia']  = ['id' => $array['data'][0]['id_provincia'], 'name' => $array['data'][0]['name_provincia']];
    $data['comuna']     = ['id' => $array['data'][0]['address_city_id'], 'name' => $array['data'][0]['name_comuna']];
    $data['telefono']   = $array['data'][0]['phone'];
    $data['celular']    = $array['data'][0]['mobile_phone'];
    $data['email']      = $array['data'][0]['email'];
    $data['sucursal']   = "";
    $data['email_suc']  = "";
    $data['btn']    = false; //btn eliminar    
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("rhPartnerViewForRut: ", $data);
}

echo json_encode($data);