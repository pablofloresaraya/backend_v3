<?php

header('Content-Type: application/json');

try{
    $logger->info("engBaseCurrencyDelete init");
    $data = $engine->engBaseCurrencyDelete($php_input);
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engBaseCurrencyDelete: ".$data);
}

echo json_encode($data);