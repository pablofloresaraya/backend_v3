<?php

try{
    $logger->info("rhPartnerListSupplier init");
    $data = $engine->rhPartnerListSupplier();
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("rhPartnerListSupplier: ".$data);
}
header('Content-Type: application/json');
echo json_encode($data);