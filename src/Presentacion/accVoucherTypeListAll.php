<?php
try{
    $logger->info("accVoucherTypeListAll init");
    $data = $contabilidad->accVoucherTypeListAll();
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engGetModuleAll: ".$data);
}
header('Content-Type: application/json');
echo json_encode($data);
?>