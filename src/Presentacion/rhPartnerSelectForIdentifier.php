<?php
try{
    $logger->info("rhPartnerSelectForIdentifier init");
    $p_identifier = (empty($_POST["var1"]))? "":(string) $_POST["var1"];
    $data = $engine->rhPartnerSelectForIdentifier($p_identifier);
}
catch(Exception $e) {
    $data["status"] = 'error';
    $data["errores"][] = $e->getMessage();
    $data["message"] = "";
    $data["data"] = array();
    $logger->error("rhPartnerSelectForIdentifier: ".$data);
}
header('Content-Type: application/json');
echo json_encode($data);
?>