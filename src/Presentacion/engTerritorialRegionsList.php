<?php
try{

    $logger->info("engTerritorialRegionsListAll init");    
    $regions    = $engine->baseTerritorialRegionsListAll();
    $data = $regions;

}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engTerritorialRegionsListAll: ", $data);
}
header('Content-Type: application/json');
echo json_encode($data);