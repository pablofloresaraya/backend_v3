<?php

try{
    $logger->info("engBaseCurrencyView init");
    $data = $engine->engBaseCurrencyView();
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engBaseCurrencyView: ".$data);
}
header('Content-Type: application/json');
echo json_encode($data);