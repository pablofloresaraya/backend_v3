<?php

try{
    $logger->info("engBaseValueChangeView init");
    $data = $engine->engBaseValueChangeView();
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engBaseValueChangeView: ".$data);
}
header('Content-Type: application/json');
echo json_encode($data);