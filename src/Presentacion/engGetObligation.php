<?php

header('Content-Type: application/json');

try{
    $logger->info("engGetObligation init");
    $data = $engine->engGetObligation($php_input);
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engGetObligation: ".$data);
}

echo json_encode($data);