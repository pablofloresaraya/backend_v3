<?php
try{
    $logger->info("accAccountingConveniosNoContabilizados init");
    $data = $contabilidad->accConveniosNoContabilizados($_POST);
}
catch(Exception $e) {
    $data["header"] = 'error';
    $data["status"] = 'error';
    $data["errores"] = array($e->getMessage());
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("accAccountingConveniosNoContabilizados: ", $data);
}
header('Content-Type: application/json');
echo json_encode($data);
?>