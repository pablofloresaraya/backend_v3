<?php
$logger->info("accAccountingEntryGet init");
$data = $contabilidad->accEntryView($_GET);

$datos = $data["data"];
$entry = $datos["entry"];
$entry_detail = $datos["entry_detail"];
$voucher_type = $datos["voucher_type"];
$company = $datos["company"];

// reference the Dompdf namespace
use Dompdf\Dompdf;

$html = "
<html>
    <head>

<style>

@page {
    margin: 95px 40px;
}

header {
    position: fixed;
    top: -90px;
    left: 0px;
    right: 0px;
    height: 93px;

    /** Extra personal styles **/
    background-color: white;
    color: black;
    /*text-align: center;*/
    line-height: 15px;
}

footer {
    position: fixed; 
    bottom: -60px; 
    left: 0px; 
    right: 0px;
    height: 50px; 

    /** Extra personal styles **/
    background-color: white;
    color: black;
    text-align: center;
    line-height: 35px;
}

.table{
    width:100%;
    border-collapse: collapse;
}

.table thead tr th{
    font-size: 14px;
}

.table tbody tr td{
    font-size: 12px;
}
</style>
</head>
<body>
    <header>        
        &nbsp;".$company["name"]."
        <table>
            <tr><td>R.U.T.</td><td>:</td><td>".$company["identifier"]."-".$company["identifier_dv"]."</td></tr>
            <tr><td>GIRO</td><td>:</td><td>".$company["company_activity"]."</td></tr>
            <tr><td>DIRECCION</td><td>:</td><td>".$company["address"]."</td></tr>
            <tr><td>CIUDAD</td><td>:</td><td>".$company["city_name"]."</td></tr>
        </table>
    </header>

    <footer>
        &nbsp;
    </footer>

    <main>
        <center>
            <h3 style='margin: 5px;'>COMPROBANTE CONTABLE</h3>
            <span style='font-size: 18px; line-height: 20px;'>N°: ".$entry["entry_code"]." - Tipo: ".$voucher_type["name"]." - Fecha: ".$entry["entry_date_format"]."</span>            
        </center>
        <br />
        <span style='font-size: 14px; line-height: 20px;'><strong>GLOSA</strong>: ".$entry["description"]."</span>
        <br />
        <br />
        <center>
        <table class='table'>
            <thead>
                <tr><th width='150'>Cuenta</th><th width='50'>Tipo Dcto.</th><th width='40'>Nro.</th><th>Glosa</th><th width='60'>DEBITO</th><th width='60'>CREDITO</th></tr>
            </thead>
            <tbody>";

$detalle = "";
for($i=0;$i<count($entry_detail);$i++){
    $account = $entry_detail[$i]["account"];
    $detalle .= "<tr>
                    <td>".$account["account_code"]." ".$account["name"]."</td>
                    <td style='text-align: center;'>&nbsp;</td>
                    <td style='text-align: center;'>".$entry_detail[$i]["current_document_id"]."</td>
                    <td>".$entry_detail[$i]["description"]."</td>
                    <td style='text-align: right;'>".number_format($entry_detail[$i]["debit"],0,",",".")."</td>
                    <td style='text-align: right;'>".number_format($entry_detail[$i]["credit"],0,",",".")."</td>
                </tr>";
}
$html .= $detalle;
$html .= "  <tr>
                <td colspan='4' style='text-align: right; font-size: 14px;'><strong>TOTAL COMPROBANTE</strong></td>
                <td style='text-align: right;'><strong>".number_format($entry["debit"],0,",",".")."</strong></td>
                <td style='text-align: right;'><strong>".number_format($entry["credit"],0,",",".")."</strong></td>
            </tr>
        ";
$html . "
            </tbody>
        </table>
        <center>
    </main>
</body>
</html>
";
//echo $html;
//exit;
// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('letter', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream("comp.pdf", ['Attachment' => false]);
?>