<?php

header('Content-Type: application/json');

try{
    $logger->info("engBaseValueChangeSave init");
    $data = $engine->engBaseValueChangeSave($php_input);
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engBaseValueChangeSave: ".$data);
}

echo json_encode($data);