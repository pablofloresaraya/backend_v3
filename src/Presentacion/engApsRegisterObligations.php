<?php

try{
    $logger->info("engRegisterObligationsSave init");
    $data = $engine->engRegisterObligationsSave($php_input);
}
catch(Exception $e) {
    $data["header"] = 'ERROR';
    $data["status"] = 'ERROR';
    $data["message"] = $e->getMessage();
    $data["data"] = array();
    $logger->error("engRegisterObligationsSave: ".$data);
}
header('Content-Type: application/json');
echo json_encode($data);