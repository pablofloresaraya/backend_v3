<?php
declare(strict_types=1);

namespace App\Datos\Clinica;

use Psr\Log\LoggerInterface;
use App\Datos\Connection\DataBaseClinic;

class ClinicaSql{

    //private $dataBase;
    protected $logger;

    public function __construct(LoggerInterface $logger){
        $this->logger = $logger;
    }

    //paciente
    public function getPaciente($p_pc_cod){
        $dataBase = new DataBaseClinic($this->logger);
        $sql = "SELECT a.*
                FROM pacientes a
                WHERE a.pc_cod = :p_pc_cod;";
                
        $param = array(':p_pc_cod' => $p_pc_cod);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);

        return $data;   
    }

    //presupuesto
    public function getPresupuesto($p_ps_cod){
        $dataBase = new DataBaseClinic($this->logger);
        $sql = "SELECT a.*
                FROM presupuesto a
                WHERE a.ps_cod = :p_ps_cod;";
                
        $param = array(':p_ps_cod' => $p_ps_cod);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);

        return $data;   
    }

    //oexterna
    public function getOExterna($p_oe_cod){
        $dataBase = new DataBaseClinic($this->logger);
        $sql = "SELECT a.*
                FROM oexterna a
                WHERE a.oe_cod = :p_oe_cod;";
                
        $param = array(':p_oe_cod' => $p_oe_cod);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);

        return $data;   
    }

    //odental
    public function getODental($p_od_cod){
        $dataBase = new DataBaseClinic($this->logger);
        $sql = "SELECT a.*
                FROM odental a
                WHERE a.od_cod = :p_od_cod;";
                
        $param = array(':p_od_cod' => $p_od_cod);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);

        return $data;   
    }
}