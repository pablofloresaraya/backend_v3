<?php
declare(strict_types=1);

namespace App\Datos\Contabilidad;

use Psr\Log\LoggerInterface;
use App\Datos\Connection\DataBaseKlinik;

class ContabilidadSql{

    //private $dataBase;
    protected $logger;

    public function __construct(LoggerInterface $logger){
        //$this->dataBase = new DataBaseKlinik();
        $this->logger = $logger;
    }

    //acc_period
    public function accPeriodOpenForDate($p_company_id,$p_date){
        $dataBase = new DataBaseKlinik($this->logger);
        $sql = "SELECT *
                FROM acc_period 
                WHERE company_id = :p_company_id 
                AND :p_date between start_date and end_date
                AND status=true;";
                
        $param = array(':p_company_id' => $p_company_id,':p_date' => $p_date);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);

        return $data;   
    }

    //Acc_VoucherType
    public function accVoucherTypeListAll(int $p_company_id): array{        
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "select id, company_id, voucher_type_code, name, description, class_type, create_uid, create_date
                from acc_voucher_type as a
                where company_id=:p_company_id
                order by id;";
                
        $param = array(':p_company_id' => $p_company_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function accVoucherTypeView(int $p_id): array{        
        $dataBase = new DataBaseKlinik($this->logger);
        
        $sql = "SELECT * FROM acc_voucher_type WHERE id=:p_id";

        $param = array(':p_id' => $p_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function accVoucherTypeUpdate(int $p_id, int $p_company_id, string $p_voucher_type_code, string $p_name, string $p_description, string $p_class_type): bool{        
        $dataBase = new DataBaseKlinik($this->logger);
        
        $sql = "UPDATE acc_voucher_type 
                SET company_id=:p_company_id, voucher_type_code=:p_voucher_type_code, name=:p_name, description=:p_description, class_type=:p_class_type
                WHERE id=:p_id";
        
        $param = array(':p_id' => $p_id,':p_company_id' => $p_company_id,':p_voucher_type_code' => $p_voucher_type_code,':p_name' => $p_name,':p_description' => $p_description,':p_class_type' => $p_class_type);
        $result = $dataBase->execQueryParam("UPDATE",$sql,$param);
        return $result;
    }

    public function accVoucherTypeDelete(int $p_id): array{        
        $dataBase = new DataBaseKlinik($this->logger);
        
        $sql = "DELETE
                FROM acc_voucher_type 
                WHERE id=:p_id";
        
        $param = array(':p_id' => $p_id);
        $data = $dataBase->execQueryParam("DELETE",$sql,$param);
        return $data;
    }

    //Acc_Account
    public function accAccountView(int $p_id): array{        
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "select *
                from acc_account as a
                where id = :p_id;";
                
        $param = array(':p_id' => $p_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;   
    }

    public function accAccountViewForCode(int $p_company_id,string $p_code): array{        
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "select *
                from acc_account as a
                where 1=1
                and a.company_id=:p_company_id
                and a.account_code=:p_code;";
                
        $param = array(':p_code' => $p_code, ':p_company_id' => $p_company_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;   
    }

    public function accAccountListAll(int $p_company_id,string $p_nombre): array{        
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "select  regexp_matches(upper(a.name),upper(:p_nombre))
                    ,a.*,b.name type_name
                from acc_account as a
                left outer join acc_account_type as b on b.id=a.type_id
                where 1=1
                and a.company_id=:p_company_id
                order by a.account_code;";
                
        $param = array(':p_nombre' => $p_nombre, ':p_company_id' => $p_company_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;   
    }

    public function accAccountsListChargeable(): array{                                           
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "SELECT id, account_code as cuenta, name 
                FROM public.acc_account 
                WHERE type_id=2
                ORDER BY id ASC";

        $param = array();

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    //acc_entry
    public function accEntryIdSeq(){
        $dataBase = new DataBaseKlinik($this->logger);
        $sql = "SELECT nextval('acc_entry_id_seq') id;";
                
        $param = array();
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data[0]["id"];   
    }

    public function accEntryCodeCompanyYear($p_company_id,$p_entry_year){
        $dataBase = new DataBaseKlinik($this->logger);
        $sql = "SELECT entry_code
                FROM acc_entry_code_company_year 
                WHERE company_id = :p_company_id 
                AND entry_year = :p_entry_year;";
                
        $param = array(':p_company_id' => $p_company_id,':p_entry_year' => $p_entry_year);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        
        $tipo_sentencia = "";
        if(count($data)==0){
            $entry_code = 1;
            $tipo_sentencia = "INSERT";
            $sql = "INSERT 
                    INTO acc_entry_code_company_year 
                            (company_id, entry_year, entry_code)
                    VALUES  (:p_company_id, :p_entry_year, :p_entry_code);";
        }
        else{
            $entry_code = $data[0]["entry_code"] + 1;

            $tipo_sentencia = "UPDATE";
            $sql = "UPDATE acc_entry_code_company_year 
                    SET entry_code = :p_entry_code
                    WHERE company_id = :p_company_id 
                    and entry_year = :p_entry_year;";
        }

        $param = array(':p_company_id' => $p_company_id,':p_entry_year' => $p_entry_year, ":p_entry_code" => $entry_code);
        $data = $dataBase->execQueryParam($tipo_sentencia,$sql,$param);

        return $entry_code;   
    }

    //public function accEntryInsert($p_id, $p_company_id, $p_entry_code
    //                , $p_entry_year, $p_entry_month, $p_entry_date, $p_description
    //                , $p_debit, $p_credit, $p_voucher_type_id
    //                , $p_create_uid, $p_create_module){
    public function accEntryInsert($p_param): bool{
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "INSERT 
                INTO acc_entry
                        ( id, company_id, entry_code
                        , entry_year, entry_month, entry_date, description
                        , debit, credit, voucher_type_id
                        , create_uid, create_date, create_module)
                VALUES  ( :p_id, :p_company_id, :p_entry_code
                        , :p_entry_year, :p_entry_month, :p_entry_date, :p_description
                        , :p_debit, :p_credit, :p_voucher_type_id
                        , :p_create_uid, now(), :p_create_module);";
                
        //$param = array(':p_id' => $p_id, ':p_company_id' => $p_company_id, ':p_entry_code' => $p_entry_code
        //                , ':p_entry_year' => $p_entry_year, ':p_entry_month' => $p_entry_month, ':p_entry_date' => $p_entry_date, ':p_description' => $p_description
        //                , ':p_debit' => $p_debit, ':p_credit' => $p_credit, ':p_voucher_type_id' => $p_voucher_type_id
        //                , ':p_create_uid' => $p_create_uid, ':p_create_module' => $p_create_module);

        $data = $dataBase->execQueryParam("INSERT",$sql,$p_param);
        return $data;   
    }

    //public function accEntryUpdate($p_id, $p_entry_year, $p_entry_month, $p_entry_date
    //                , $p_description, $p_debit, $p_credit, $p_voucher_type_id){
    public function accEntryUpdate($p_param): bool{
        $dataBase = new DataBaseKlinik($this->logger);

        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "UPDATE acc_entry
                SET entry_year = :p_entry_year
                    entry_month = :p_entry_month
                    entry_date = :p_entry_date
                    description = :p_description
                    debit = :p_debit
                    credit = :p_credit
                    voucher_type_id = :p_voucher_type_id
                WHERE id = :p_id;";
                
        //$param = array(':p_id' => $p_id, ':p_entry_year' => $p_entry_year, ':p_entry_month' => $p_entry_month, ':p_entry_date' => $p_entry_date
        //            , ':p_description' => $p_description, ':p_debit' => $p_debit, ':p_credit' => $p_credit, ':p_voucher_type_id' => $p_voucher_type_id);

        $data = $dataBase->execQueryParam("UPDATE",$sql,$p_param);
        return $data;   
    }

    public function accEntryDelete($p_id,$p_user_id): bool{
        $dataBase = new DataBaseKlinik($this->logger);

        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "UPDATE acc_entry
                SET removed = true,
                    remove_uid = :p_user_id,
                    remove_date = now()
                WHERE id = :p_id;";
                
        $param = array(':p_id' => $p_id, ':p_user_id' => $p_user_id);

        $data = $dataBase->execQueryParam("UPDATE",$sql,$param);
        return $data;   
    }

    public function accEntryDetailDeteleAll($p_entry_id){

        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "DELETE
                FROM acc_entry_detail
                WHERE entry_id = :p_entry_id;";
                
        $param = array(':p_entry_id' => $p_entry_id);

        $data = $dataBase->execQueryParam("INSERT",$sql,$param);
        return $data;   
    }

    //public function accEntryDetailSave($p_entry_id, $p_line, $p_account_id
    //                , $p_debit, $p_credit
    //                , $p_description, $p_cost_center_id, $p_partner_id
    //                , $p_initial_document_id, $p_current_document_id){
    public function accEntryDetailSave($p_param): bool{

        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "INSERT 
                INTO acc_entry_detail
                        ( entry_id, line, account_id
                        , debit, credit
                        , description, cost_center_id, partner_id
                        , initial_document_id, current_document_id)
                VALUES  ( :p_entry_id, :p_line, :p_account_id
                        , :p_debit, :p_credit
                        , :p_description, :p_cost_center_id, :p_partner_id
                        , :p_initial_document_id, :p_current_document_id);";
                
        //$param = array(':p_entry_id' => $p_entry_id, ':p_line' => $p_line, ':p_account_id' => $p_account_id
        //                , ':p_debit' => $p_debit, ':p_credit' => $p_credit
        //                , ':p_description' => $p_description, ':p_cost_center_id' => $p_cost_center_id, ':p_partner_id' => $p_partner_id
        //                , ':p_initial_document_id' => $p_initial_document_id, ':p_current_document_id' => $p_current_document_id);

        $data = $dataBase->execQueryParam("INSERT",$sql,$p_param);
        return $data;   
    }

    public function accEntryView($p_id){

        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "SELECT a.*,to_char(entry_date,'dd-mm-yyyy') entry_date_format 
                FROM acc_entry a
                WHERE id = :p_id;";

        $param = array(":p_id" => $p_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;   
    }

    public function accEntryViewForCompanyCodeYear($p_company_id, $p_entry_code, $p_entry_year){

        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "SELECT a.*,to_char(entry_date,'dd-mm-yyyy') entry_date_format 
                FROM acc_entry a
                WHERE company_id = :p_company_id
                AND entry_code = :p_entry_code
                AND entry_year = :p_entry_year;";

        $param = array(":p_company_id" => $p_company_id, ":p_entry_code" => $p_entry_code, ":p_entry_year" => $p_entry_year);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;   
    }

    public function accEntryDetailView($p_entry_id){

        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "SELECT * 
                FROM acc_entry_detail
                WHERE entry_id = :p_entry_id;";

        $param = array(":p_entry_id" => $p_entry_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;   
    }

    //acc_in_pagares
    public function accInPagaresIdSeq(){
        $dataBase = new DataBaseKlinik($this->logger);
        $sql = "SELECT nextval('acc_in_pagares_id_seq') id;";
                
        $param = array();
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data[0]["id"];   
    }

    public function accInPagaresInsert($p_param): bool{
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "INSERT 
                INTO acc_in_pagares
                        ( id, company_id
                        , create_uid, create_date)
                VALUES  ( :p_id, :p_company_id
                        , :p_create_uid, now());";

        $data = $dataBase->execQueryParam("INSERT",$sql,$p_param);
        return $data;   
    }

    public function accInPagaresDetailInsert($p_param): bool{
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "INSERT 
                INTO acc_in_pagares_detail
                        ( in_pagares_id, ipg_cod, ipg_cuota, ipg_valor, entry_id )
                VALUES  ( :p_in_pagares_id, :p_ipg_cod, :p_ipg_cuota, :p_ipg_valor, :p_entry_id );";

        $data = $dataBase->execQueryParam("INSERT",$sql,$p_param);
        return $data;   
    }

    public function accInPagaresDetailUpdateForIpgCod($p_in_pagares_id,$p_ipg_cod,$p_entry_id): bool{
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "UPDATE acc_in_pagares_detail
                SET entry_id = :p_entry_id
                WHERE in_pagares_id = :p_in_pagares_id
                AND ipg_cod = :p_ipg_cod;";
                
        $param = array(':p_in_pagares_id' => $p_in_pagares_id, ':p_ipg_cod' => $p_ipg_cod, ':p_entry_id' => $p_entry_id);

        $data = $dataBase->execQueryParam("UPDATE",$sql,$param);
        return $data;   
    }

    public function accInPagaresDetailForIpgCod($p_ipg_cod): array{
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "SELECT a.*,b.entry_code
                FROM acc_in_pagares_detail a,acc_entry b
                WHERE ipg_cod = :p_ipg_cod
                AND b.id=a.entry_id 
                AND b.removed = false;";
                
        $param = array(':p_ipg_cod' => $p_ipg_cod);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;   
    }

    //acc_in_convenios
    public function accInConveniosIdSeq(){
        $dataBase = new DataBaseKlinik($this->logger);
        $sql = "SELECT nextval('acc_in_convenios_id_seq') id;";
                
        $param = array();
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data[0]["id"];   
    }

    public function accInConveniosInsert($p_param): bool{
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "INSERT 
                INTO acc_in_convenios
                        ( id, company_id
                        , create_uid, create_date)
                VALUES  ( :p_id, :p_company_id
                        , :p_create_uid, now());";

        $data = $dataBase->execQueryParam("INSERT",$sql,$p_param);
        return $data;   
    }

    public function accInConveniosDetailInsert($p_param): bool{
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "INSERT 
                INTO acc_in_convenios_detail
                        ( in_convenios_id, ic_cod, ic_cuota, ic_valor, entry_id )
                VALUES  ( :p_in_convenios_id, :p_ic_cod, :p_ic_cuota, :p_ic_valor, :p_entry_id );";

        $data = $dataBase->execQueryParam("INSERT",$sql,$p_param);
        return $data;   
    }

    public function accInConveniosDetailForIcCod($p_ic_cod): array{
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "SELECT a.*,b.entry_code
                FROM acc_in_convenios_detail a,acc_entry b
                WHERE ic_cod = :p_ic_cod
                AND b.id=a.entry_id 
                AND b.removed = false;";
                
        $param = array(':p_ic_cod' => $p_ic_cod);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;   
    }

    public function accValDateReportBookBig(string $p_fechaini, string $p_fechafin): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select id from acc_entry where to_char(entry_date,'dd-mm-yyyy') between :p_fechaini and :p_fechafin";

        $param = array(':p_fechaini' => $p_fechaini, ':p_fechafin' => $p_fechafin);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function accValAccountReportBookBigDet(int $p_id, int $p_account): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select account_id from acc_entry_detail where entry_id=:p_id and account_id=:p_account";

        $param = array(':p_id' => $p_id,':p_account' => $p_account);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function accValPeriodReportBookBig(string $p_periodo): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select id from acc_entry where to_char(entry_date,'mm-yyyy') = :p_periodo";

        $param = array(':p_periodo' => $p_periodo);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function accValReportDiaryBookDate(string $p_fechaini, string $p_fechafin): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select count(*) as reports from acc_entry where to_char(entry_date,'dd-mm-yyyy') between :p_fechaini and :p_fechafin";

        $param = array(':p_fechaini' => $p_fechaini,':p_fechafin' => $p_fechafin);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function accValReportDiaryBookPeriod(string $p_periodo): array{                                            
        $dataBase = new DataBaseKlinik($this->logger); 

        $sql = "select count(*) as reports 
                from acc_entry a where to_char(a.entry_date,'mm-yyyy') = :p_periodo";

        $param = array(':p_periodo' => $p_periodo);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }
}