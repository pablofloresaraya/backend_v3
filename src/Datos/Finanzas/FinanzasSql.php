<?php
declare(strict_types=1);

namespace App\Datos\Finanzas;

use Psr\Log\LoggerInterface;
use App\Datos\Connection\DataBaseClinic;

class FinanzasSql{

    //private $dataBase;
    protected $logger;

    public function __construct(LoggerInterface $logger){
        $this->logger = $logger;
    }

    //ing_pagares
    public function getPagare($p_ipg_cod){
        $dataBase = new DataBaseClinic($this->logger);
        $sql = "select ipg_cod,ipg_fecha,bc_cod,ipg_num,ipg_ncuota,ipg_interes,ipg_cartola,ipg_vcuota,us_firma,ipg_obs,ipg_total,ipg_nula,ipg_fpago,ipg_deudor_rut,ipg_deudor_dv,ipg_deudor_nombre,ipg_deudor_domicilio
                from ing_pagares 
                where ipg_cod = :p_ipg_cod;";
                
        $param = array(':p_ipg_cod' => $p_ipg_cod);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);

        return $data;   
    }

    public function getPagareCuotas($p_ipg_cod){
        $dataBase = new DataBaseClinic($this->logger);
        $sql = "select distinct b.dtmv_cuota,b.dtmv_vence,b.dtmv_valorpag
                from movimientos a,dt_movimientos as b 
                where a.mv_numero=0
                and a.mv_ntping = :p_ipg_cod
                and a.mv_tping = 7
                and b.mv_cod = a.mv_cod
                order by b.dtmv_vence;";
                
        $param = array(':p_ipg_cod' => $p_ipg_cod);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);

        return $data;   
    }

    public function getPagareDetalle($p_ipg_cod){
        $dataBase = new DataBaseClinic($this->logger);
        $sql = "select ep_num,formapago,ps_cod,ep_tipo,ep_total
                from estadopago 
                where ep_num = :p_ipg_cod
                and formapago=7
                and ep_tipo=1;";
                
        $param = array(':p_ipg_cod' => $p_ipg_cod);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);

        return $data;   
    }

    public function getConvenioCuotas($p_ic_cod,$p_ic_oatencion,$p_ic_cuotas){
        $dataBase = new DataBaseClinic($this->logger);
        $sql = "select b.dtmv_cuota,b.dtmv_vence,sum(b.dtmv_valor) dtmv_valor
                from movimientos a,dt_movimientos as b 
                where a.mv_numero=0
                and a.mv_ntping = :p_ic_cod
                and a.mv_norden = :p_ic_oatencion
                and a.mv_cuota = :p_ic_cuotas
                and a.mv_tping = 2
                and b.mv_cod = a.mv_cod
                group by b.dtmv_cuota,b.dtmv_vence
                order by b.dtmv_vence;";
                
        $param = array(':p_ic_cod' => $p_ic_cod, ':p_ic_oatencion' => $p_ic_oatencion,':p_ic_cuotas' => $p_ic_cuotas);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);

        return $data;   
    }

    public function getConvenioDetalle($p_ic_cod){
        $dataBase = new DataBaseClinic($this->logger);
        $sql = "select ep_num,formapago,ps_cod,ep_tipo,ep_total
                from estadopago 
                where ep_num = :p_ic_cod
                and formapago=2;";
                
        $param = array(':p_ic_cod' => $p_ic_cod);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);

        return $data;   
    }

    public function buscarConveniosNoNulosForFecha(string $p_fecdesde,string $p_fechasta){
        $dataBase = new DataBaseClinic($this->logger);
        
        $sql = "SELECT a.*,(select b.tcv_nombre from tp_convenio b where b.tcv_cod=a.tcv_cod) tcv_nombre
                from ing_convenios a
                where ic_fecha between :p_fecdesde and :p_fechasta
                and ic_nula=false
                order by ic_cod desc;";
                
        $param = array(':p_fecdesde' => $p_fecdesde,':p_fechasta' => $p_fechasta);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);

        return $data;   
    }

    public function buscarConveniosNoNulosForPeriodo($p_mes,$p_anho){
        $dataBase = new DataBaseClinic($this->logger);
        $sql = "SELECT a.*,(select b.tcv_nombre from tp_convenio b where b.tcv_cod=a.tcv_cod) tcv_nombre
                from ing_convenios a
                where date_part('year',ic_fecha)=:p_anho
                and date_part('month',ic_fecha)=:p_mes
                and ic_nula=false
                order by ic_cod desc;";
                
        $param = array(':p_mes' => $p_mes,':p_anho' => $p_anho);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);

        return $data;   
    }

    public function buscarPagaresNoNulosForFecha(string $p_fecdesde,string $p_fechasta){
        $dataBase = new DataBaseClinic($this->logger);
        
        $sql = "SELECT *
                from ing_pagares
                where ipg_fecha between :p_fecdesde and :p_fechasta
                and ipg_nula=false
                order by ipg_cod desc;";
                
        $param = array(':p_fecdesde' => $p_fecdesde,':p_fechasta' => $p_fechasta);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);

        return $data;   
    }

    public function buscarPagaresNoNulosForPeriodo($p_mes,$p_anho){
        $dataBase = new DataBaseClinic($this->logger);
        $sql = "SELECT *
                from ing_pagares
                where date_part('year',ipg_fecha)=:p_anho
                and date_part('month',ipg_fecha)=:p_mes
                and ipg_nula=false
                order by ipg_cod desc;";
                
        $param = array(':p_mes' => $p_mes,':p_anho' => $p_anho);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);

        return $data;   
    }
}