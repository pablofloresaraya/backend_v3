<?php
namespace App\Datos\Connection;
use Psr\Log\LoggerInterface;
use \PDO;
use Dotenv\Dotenv;

class DataBaseClinic{

    protected $connection;
    protected $logger;

    public function __construct(LoggerInterface $logger){

        //$dotenv = Dotenv::createImmutable(__DIR__ . "/../../../");
        //$dotenv->load();

        $dbSettings['dbname'] = $_ENV["CLINIC_DB_NAME"];
        $dbSettings['user'] = $_ENV["CLINIC_DB_USER"];
        $dbSettings['pass'] = $_ENV["CLINIC_DB_PASSWORD"];
        $dbSettings['host'] = $_ENV["CLINIC_DB_HOST"];

        $this->logger = $logger;

        try{
            $pdo = new PDO("pgsql:host=" . $dbSettings['host'] . ";dbname=" . $dbSettings['dbname'], $dbSettings['user'], $dbSettings['pass']);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);            
            $this->logger->info("Connection ".$dbSettings['dbname']." Ok");
        }
        catch(\PDOException $e){
            $this->logger->error("Connection ".$dbSettings['dbname']." failed: ".$e->getMessage());
        }
        
        $this->connection = $pdo;
    }
   
    public function getConnection(){
        return $this->connection;
    }
    
    public function execQuery($sql){        
        $conn = $this->connection;
        $result = $conn->query($sql);
        
        $data = array();
        $i=0;
        foreach ($result as $row) {
            $data[$i] = $row;
            $i++;
        }
        return $data;
    }
    
    public function execQueryParamSelect($sql,$param){        
        $conn = $this->connection;
        $sth = $conn->prepare($sql);
        $sth->execute($param);
        $result = $sth->fetchAll();
        return $result;
    }
    
    public function execQueryParam($type,$sql,$param){        
        $conn = $this->connection;
        $sth = $conn->prepare($sql);
        if($sth->execute($param)){
            if($type=="SELECT"){            
                $result = $sth->fetchAll();
                return $result;
            }
            else{
                return true;
            }
        }
        else{
            return false;
        }
    }
}

?>