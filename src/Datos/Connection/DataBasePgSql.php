<?php
namespace App\Datos\Connection;
use \PDO;
use Dotenv\Dotenv;

class DataBasePgSql{

    protected $connection;

    public function __construct(){

        $dotenv = Dotenv::createImmutable(__DIR__ . "/../../../../");
        $dotenv->load();

        $dbSettings['dbname'] = getenv("KLINIK_DB_NAME");
        $dbSettings['user'] = getenv("KLINIK_DB_USER");
        $dbSettings['pass'] = getenv("KLINIK_DB_PASSWORD");
        $dbSettings['host'] = getenv("KLINIK_DB_HOST");

        try{
            $pdo = new PDO("pgsql:host=" . $dbSettings['host'] . ";dbname=" . $dbSettings['dbname'], $dbSettings['user'], $dbSettings['pass']);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);            
            //$this->logger->info("Connection Ok");
        }
        catch(\PDOException $e){
            //$this->logger->info("Connection failed: ".$e->getMessage());
            //echo "Connection failed: ".$e->getMessage();
        }
        
        $this->connection = $pdo;
    }
   
    public function getConnection(){
        return $this->connection;
    }
    
    public function execQuery($sql){        
        $conn = $this->connection;
        $result = $conn->query($sql);
        
        $data = array();
        $i=0;
        foreach ($result as $row) {
            $data[$i] = $row;
            $i++;
        }
        return $data;
    }
    
    public function execQueryParam($sql,$param){        
        $conn = $this->connection;
        $sth = $conn->prepare($sql);
        $sth->execute($param);
        $result = $sth->fetchAll();
        return $result;
    }
}

?>