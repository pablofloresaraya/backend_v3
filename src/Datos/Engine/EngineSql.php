<?php
declare(strict_types=1);

namespace App\Datos\Engine;

use Psr\Log\LoggerInterface;
use App\Datos\Connection\DataBaseKlinik;

class EngineSql{

    //private $dataBase;
    protected $logger;

    public function __construct(LoggerInterface $logger){
        //$this->dataBase = new DataBaseKlinik();
        $this->logger = $logger;
    }

    public function getModules(string $p_type): array{   
        $dataBase = new DataBaseKlinik($this->logger);
        
        $sql = "select id,name,description,is_active,parent_id,module_id,menu_type,menu_order
                from base_ui_menu
                where parent_id is null
                and menu_type=:p_type
                and is_active=true
                order by menu_order";

        $param = array(':p_type' => $p_type);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function getModuleAll(int $p_module): array{   
        $dataBase = new DataBaseKlinik($this->logger);
        
        $sql = "select id,name,description,is_active,parent_id,module_id,menu_type,menu_order
                from base_ui_menu
                where module_id=:p_module
                and is_active=true
                and parent_id is not null
                order by menu_order, id";

        $param = array(':p_module' => $p_module);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function getModuleSections(int $p_module): array{   
        $dataBase = new DataBaseKlinik($this->logger);
        
        $sql = "select id,name,description,is_active,parent_id,module_id,menu_type,menu_order
                from base_ui_menu
                where module_id=:p_module
                and parent_id=:p_module
                and is_active=true
                order by menu_order";

        $param = array(':p_module' => $p_module);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function getModuleSectionMenu(int $p_module, int $p_section): array{     
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "select id,name,description,is_active,parent_id,module_id,menu_type,menu_order
                from base_ui_menu
                where module_id=:p_module
                and parent_id=:p_section
                and is_active=true
                order by menu_order";

        $param = array(':p_module' => $p_module, ':p_section' => $p_section);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function baseUserForUserName(string $p_username): array{     
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "select a.*,b.name 
                from base_user a
                left outer join rh_partner as b
                    on b.user_id=a.id
                where login = :p_username";

        $param = array(':p_username' => $p_username);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function baseUserLogin(string $p_username,string $p_password): array{     
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "select a.*,b.name 
                from base_user a
                left outer join rh_partner as b
                    on b.user_id=a.id
                where login = :p_username
                and password = :p_password";

        $param = array(':p_username' => $p_username, ':p_password' => $p_password);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function baseUserCompanySelectAllForUser(int $p_user_id): array{     
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "select a.company_id,b.partner_id,c.name,c.identifier,c.identifier_dv
                from base_user_company a
                left outer join base_company b
                    on b.id=a.company_id
                left outer join rh_partner c
                    on c.id=b.partner_id
                where a.user_id=:p_user_id
                order by b.name";

        $param = array(':p_user_id' => $p_user_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function baseUserCompanyDefaultForUser(int $p_user_id): array{     
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "select a.company_id,b.partner_id,c.name,c.identifier,c.identifier_dv
                from base_user_company a
                left outer join base_company b
                    on b.id=a.company_id
                left outer join rh_partner c
                    on c.id=b.partner_id
                where a.user_id=:p_user_id
                and a.company_default=true";

        $param = array(':p_user_id' => $p_user_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function baseCompanySelect(int $p_id): array{     
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "select a.*,b.identifier,b.identifier_dv,b.company_activity,b.address,c.name city_name
                from base_company a
                left outer join rh_partner as b
                    on b.id=a.partner_id
                left outer join base_territorial as c
                    on c.id=b.address_city_id
                where a.id = :p_id";

        $param = array(':p_id' => $p_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function rhPartnerViewForRut(string $p_rut): array{  //proveedor-clientes 
        $dataBase = new DataBaseKlinik($this->logger);     
        
        $sql = "select a.*
                ,(select name from base_territorial where id = a.address_city_id and char_length(code)=5) as name_comuna
		        ,(select parent_id from base_territorial where id = a.address_city_id and char_length(code)=5) as id_provincia
		        ,(select name from base_territorial where id = (select parent_id from base_territorial where id = a.address_city_id and char_length(code)=5) and char_length(code)=3) as name_provincia		
		        ,(select parent_id from base_territorial where id = (select parent_id from base_territorial where id = a.address_city_id and char_length(code)=5) and char_length(code)=3) as id_region
		        ,(select name from base_territorial where id = (select parent_id from base_territorial where id = (select parent_id from base_territorial where id = a.address_city_id and char_length(code)=5) and char_length(code)=3) and char_length(code)=2) as name_region	
                ,(select count(*) from arc_customer where partner_id=a.id and activo=TRUE) as customer
                ,(select count(*) from aps_supplier where partner_id=a.id and activo=TRUE) as supplier
        from rh_partner a 
            where a.identifier=:p_rut";
                   
        $param = array(':p_rut' => $p_rut);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function rhPartnerListCustormesSuppliers(int $p_company_id): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "select a.id, a.identifier as run, a.identifier_dv, (a.identifier||'-'||a.identifier_dv) as rut, a.name as nombre 
                from rh_partner a
                where company_id = :p_company_id
                and ( 0<(select count(*) 
                            from arc_customer 
                            where partner_id=a.id and activo=TRUE)
                        or 
                        0<(select count(*) 
                            from aps_supplier 
                            where partner_id=a.id and activo=TRUE) 
                    )";
                
        $param = array(":p_company_id" => $p_company_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function rhPartnerIsSupplier(int $p_id): array{
        $dataBase = new DataBaseKlinik($this->logger);                                            
          
        $sql = "select count(*) as supplier from aps_supplier where partner_id=:p_id";

        $param = array(':p_id' => $p_id);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function rhPartnerIsCustomer(int $p_id): array{
        $dataBase = new DataBaseKlinik($this->logger);                                            
          
        $sql = "select count(*) as customer from arc_customer where partner_id=:p_id";

        $param = array(':p_id' => $p_id);

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;

    }

    public function rhPartnerSupplierDelete(int $p_id): bool{
        $dataBase = new DataBaseKlinik($this->logger);                                            
          
        $sql = "update aps_supplier set activo=false where partner_id=:p_id";

        $param = array(':p_id' => $p_id);

        $data = $dataBase->execQueryParam("UPDATE",$sql,$param);
        return $data;

    }

    public function rhPartnerCustomerDelete(int $p_id): bool{
        $dataBase = new DataBaseKlinik($this->logger);                                            
          
        $sql = "update arc_customer set activo=false where partner_id=:p_id";

        $param = array(':p_id' => $p_id);

        $data = $dataBase->execQueryParam("UPDATE",$sql,$param);
        return $data;

    }

    public function rhPartnerSelect(int $p_partner_id): array{     
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "select *
                from rh_partner
                where id = :p_partner_id";

        $param = array(':p_partner_id' => $p_partner_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function rhPartnerSelectForIdentifier(string $p_identifier): array{     
        $dataBase = new DataBaseKlinik($this->logger);

        $sql = "select *
                from rh_partner
                where identifier = :p_identifier";

        $param = array(':p_identifier' => $p_identifier);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function rhPartnerIdSeq(){
        $dataBase = new DataBaseKlinik($this->logger);
        $sql = "SELECT nextval('rh_partner_id_seq') id;";
                
        $param = array();
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data[0]["id"];   
    }

    public function rhPartnerSave(int $p_id,string $p_identifier,string $p_dv,string $p_name,int $p_usuario,                                int    $p_city,                               string $p_direccion,string $p_phone,string $p_mobile,string $p_email,string $p_giro,string $p_sucursal,string $p_sucursal_email): array{
        $dataBase = new DataBaseKlinik($this->logger);                                
        
        $sql = "insert into rh_partner
                (   id, identifier, 
                    identifier_dv, 
                    name,
                    user_id, 
                    address_country_id, 
                    address_city_id,
                    address,
                    phone,
                    mobile_phone,
                    email,
                    company_activity,
                    sucursal,
                    sucursal_email
                )
                values ( :p_id,
                    :p_identifier,
                    :p_dv,
                    :p_name,
                    :p_usuario,
                    (select country_id from base_territorial where id = :p_city),
                    :p_city,
                    :p_direccion,
                    :p_phone,
                    :p_mobile,
                    :p_email,
                    :p_giro,
                    :p_sucursal,
                    :p_sucursal_email                   
                )";
                   
        $param = array(
                        ':p_id' => $p_id,
                        ':p_identifier' => $p_identifier,
                        ':p_dv' => $p_dv,
                        ':p_name' => $p_name,
                        ':p_usuario' => $p_usuario,                        
                        ':p_city' => $p_city,
                        ':p_direccion' => $p_direccion,
                        ':p_phone' => $p_phone,
                        ':p_mobile' => $p_mobile,
                        ':p_email' => $p_email,
                        ':p_giro' => $p_giro,
                        ':p_sucursal' => $p_sucursal,
                        ':p_sucursal_email' => $p_sucursal_email                         
                    );
        $data = $dataBase->execQueryParam("INSERT",$sql,$param);
        return $data;      
    }

    public function rhPartnerSupplierInsert(int $p_partner): array{
        $dataBase = new DataBaseKlinik($this->logger);                                            
          
        $sql = "insert into aps_supplier (partner_id, activo) values (:p_partner, true)";

        $param = array(':p_partner' => $p_partner);

        $data = $dataBase->execQueryParam("INSERT",$sql,$param);
        return $data;

    }

    public function rhPartnerCustomerInsert(int $p_partner): array{                                           
        $dataBase = new DataBaseKlinik($this->logger);   

        $sql = "insert into arc_customer (partner_id, activo) values (:p_partner, true)";

        $param = array(':p_partner' => $p_partner);

        $data = $dataBase->execQueryParam("INSERT",$sql,$param);
        return $data;

    }

    public function rhPartnerUpdate(string $p_identifier,string $p_identifier_dv,                                    string $p_name,int $p_usuario,int $p_city,string $p_direccion,string $p_phone,string $p_mobile,string $p_email,string $p_giro,string $p_sucursal,string $p_sucursal_email,int $p_id): bool{
                                    
        $dataBase = new DataBaseKlinik($this->logger);                             

        $sql = "update rh_partner a 
                set 
                    identifier = :p_identifier,
                    identifier_dv = :p_identifier_dv,                 
                    name = :p_name,
                    user_id = :p_usuario 
                    address_country_id = (select country_id from base_territorial where id = :p_city), 
                    address_city_id = :p_city,
                    address = :p_direccion,
                    phone = :p_phone,
                    mobile_phone = :p_mobile,
                    email = :p_email,
                    company_activity = :p_giro
                    sucursal = :p_sucursal,
                    sucursal_email = :p_sucursal_email
                where id = :p_id and user_id = 1";

        $param = array(
            ':p_identifier' => $p_identifier,
            ':p_identifier_dv' => $p_identifier_dv,
            ':p_name' => $p_name,
            ':p_usuario' => $p_usuario,
            ':p_city' => $p_city,
            ':p_direccion' => $p_direccion,
            ':p_phone' => $p_phone,
            ':p_mobile' => $p_mobile,
            ':p_email' => $p_email,
            ':p_giro' => $p_giro,
            ':p_sucursal' => $p_sucursal,
            ':p_sucursal_email' => $p_sucursal_email,
            ':p_id' => $p_id
        );

        $data = $dataBase->execQueryParam("UPDATE",$sql,$param);
        return $data;      
    }

    public function rhPartnerSupplierUpdate(int $p_id, string $p_proveedor): bool{
        $dataBase = new DataBaseKlinik($this->logger);                                            
        $sql = "";
        if($p_proveedor==="T"){        
            $sql = "update aps_supplier set activo=true where partner_id = :p_id";
        }else{    
            $sql = "update aps_supplier set activo=false where partner_id = :p_id";
        }

        $param = array(':p_id' => $p_id);

        $data = $dataBase->execQueryParam("UPDATE",$sql,$param);
        return $data;

    }

    public function rhPartnerCustomerUpdate(int $p_id, string $p_cliente): bool{
        $dataBase = new DataBaseKlinik($this->logger);                                            
        
        if($p_cliente==="T"){
            $sql = "update arc_customer set activo=true  where partner_id = :p_id";
        }else{
            $sql = "update arc_customer set activo=false where partner_id = :p_id";
        }  

        $param = array(':p_id' => $p_id);

        $data = $dataBase->execQueryParam("UPDATE",$sql,$param);
        return $data;

    }

    public function baseTerritorialRegionsListAll(): array{     //regiones 
        $dataBase = new DataBaseKlinik($this->logger);  
        
        $sql = "select id, country_id, name, code from base_territorial where parent_id IS NULL order by id ASC";
                
        $param = array();
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function baseTerritorialProvincesForRegion(int $p_region): array{  
        $dataBase = new DataBaseKlinik($this->logger);      
        
        $sql = "select id, country_id, name, parent_id, code from base_territorial where char_length(code)=3 and parent_id = :p_region";
                   
        $param = array(':p_region' => $p_region);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function baseTerritorialCommunesForProvince(int $p_provincia): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "select id, country_id, name, parent_id, code from base_territorial where char_length(code)=5 and parent_id = :p_provincia";
                   
        $param = array(':p_provincia' => $p_provincia);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function engBaseCurrencyView(): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "select id, name, default_currency from base_currency where active=true order by id";
                   
        $param = array();
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function engBaseCurrencySave(string $p_name, string $p_description, string $p_value): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "insert into base_currency (id, company_id, name, description, default_currency, type_value, active)
                values ((select nextval('base_currency_id_seq')), :p_company, :p_name, :p_description, false, :p_value, true)";
                   
        $param = array(':p_company' => 1, ':p_name' => $p_name, ':p_description' => $p_description, ':p_value' => $p_value);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function engBaseCurrencyDelete(string $p_id): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "update base_currency set active=false where id=:p_id";
                   
        $param = array(':p_id' => $p_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function engBaseValueChangeView(): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "select a.id, a.value, a.year as fecha_anio
                ,concat_ws('-', LPAD(a.day::TEXT,2,'0'), LPAD(a.month::TEXT,2,'0'), a.year) as fecha
                ,concat_ws('-', LPAD(a.month::TEXT,2,'0'), a.year) as fecha_mes
                ,(select type_value from base_currency where id=a.currency_id) as type_value
                from base_value_change a order by a.id";
                   
        $param = array();
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function engBaseValueChangeDelete(int $p_id): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "delete from base_value_change where id=:p_id";
                   
        $param = array(':p_id' => $p_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function engBaseValueChangeSave(string $p_id_currency, string $p_valor, string $p_dia, string $p_mes, string $p_anio): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "insert into base_value_change (currency_id,value,day,month,year,id) values
        (:p_id_currency,:p_valor,:p_dia,:p_mes,:p_anio,(select nextval('base_value_change_currency_id_seq')) )";
                   
        $param = array(':p_id_currency' => $p_id_currency, ':p_valor' => $p_valor, ':p_dia' => $p_dia, ':p_mes' => $p_mes, ':p_anio' => $p_anio);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function rhPartnerListSupplier(): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "select a.id, a.identifier as run, a.identifier_dv as dig, (a.identifier||'-'||a.identifier_dv) as rut, a.name as nombre 
                from rh_partner a
                where company_id = 1
                and  0<(select count(*) from aps_supplier where partner_id=a.id and activo=TRUE)";
                   
        $param = array();
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function engBaseDocumentType(): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "select a.id, a.name as nombre, a.code as codigo,
                (select tax_code from base_tax where id=a.tax_id) as impuesto,
                (select COALESCE(value,0) from base_tax_value where tax_id=a.tax_id) as porcentaje
                from base_document_type a";
                   
        $param = array();
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function engBaseCostCenter(): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "select id, cost_center_code as codigo, name as nombre from base_cost_center";
                   
        $param = array();
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function engRegisterObligationsSave(
        int $p_company_id,
        int $p_partner_id,
        int $p_document_type_id,
        string $p_documento,
        string $p_document_date,
        string $p_accounting_date,
        string $p_expiration_date,       
        string $p_tax_percentage,
        int $p_current_id,
        float $p_exchange_value,
        float $p_amount_untaxed,
        float $p_amount_tax,
        float $p_amount_exempt,
        float $p_total,
        float $p_balance_due,
        float $p_amount_untaxed_local,
        float $p_amount_tax_local,
        float $p_amount_exempt_local,
        float $p_total_local,
        float $p_balance_due_local,
        string $p_description,
        int $p_cost_center_id,
        int $p_entry_id,
        int $p_creation_user_id       

    ) : array{
        $dataBase = new DataBAseKlinik($this->logger);

        $sql = "
                insert into aps_document
                (
                    id,
                    company_id,
                    partner_id,
                    document_type_id,
                    documento,
                    document_date,
                    accounting_date,
                    expiration_date,
                    tax_id,
                    tax_percentage,
                    current_id,
                    exchange_value,
                    amount_untaxed,
                    amount_tax,
                    amount_exempt,
                    total,
                    balance_due,
                    amount_untaxed_local,
                    amount_tax_local,
                    amount_exempt_local,
                    total_local,
                    balance_due_local,
                    description,
                    cost_center_id,
                    entry_id,
                    status,
                    creation_user_id,
                    creation_date
                )
                values
                (
                    (select nextval('aps_document_id_seq')),
                    :p_company_id,
                    :p_partner_id,
                    :p_document_type_id,
                    UPPER(:p_documento),
                    :p_document_date,
                    :p_accounting_date,
                    :p_expiration_date,
                    (select tax_id from base_document_type where id=:p_document_type_id),
                    :p_tax_percentage,
                    :p_current_id,
                    :p_exchange_value,
                    :p_amount_untaxed,
                    :p_amount_tax,
                    :p_amount_exempt,
                    :p_total,
                    :p_balance_due,
                    :p_amount_untaxed_local,
                    :p_amount_tax_local,
                    :p_amount_exempt_local,
                    :p_total_local,
                    :p_balance_due_local,
                    :p_description,
                    :p_cost_center_id,
                    :p_entry_id,
                    'C',
                    :p_creation_user_id,
                    (select current_date)
                )
        ";

        $param = array(
            ':p_company_id' => $p_company_id,
            ':p_partner_id' => $p_partner_id,
            ':p_document_type_id' => $p_document_type_id,
            ':p_documento' => $p_documento, 
            ':p_document_date' => $p_document_date,
            ':p_accounting_date' => $p_accounting_date,
            ':p_expiration_date' => $p_expiration_date,                    
            ':p_tax_percentage' => $p_tax_percentage,
            ':p_current_id' => $p_current_id,
            ':p_exchange_value' => $p_exchange_value,
            ':p_amount_untaxed' => $p_amount_untaxed,
            ':p_amount_tax' => $p_amount_tax,
            ':p_amount_exempt' => $p_amount_exempt,
            ':p_total' => $p_total,
            ':p_balance_due' => $p_balance_due,
            ':p_amount_untaxed_local' => $p_amount_untaxed_local,
            ':p_amount_tax_local' => $p_amount_tax_local,
            ':p_amount_exempt_local' => $p_amount_exempt_local,
            ':p_total_local' => $p_total_local,
            ':p_balance_due_local' => $p_balance_due_local,
            ':p_description' => $p_description,
            ':p_cost_center_id' => $p_cost_center_id,
            ':p_entry_id' => $p_entry_id,
            ':p_creation_user_id' => $p_creation_user_id           
        );

        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;
    }

    public function engTaxOperation(int $p_id, int $p_id_company): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "select a.tax_operation from base_tax a 
                where a.id=(select tax_id from base_document_type where id=:p_id) 
                and a.company_id=:p_id_company";
                   
        $param = array(':p_id' => $p_id, ':p_id_company' => $p_id_company);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function engPorcentajeAux(int $p_id, string $p_fecha_reg): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "select value from base_tax_value 
                where tax_id=(select tax_id from base_document_type where id=:p_id) 
                and :p_fecha_reg between start_date and end_date";
                   
        $param = array(':p_id' => $p_id,':p_fecha_reg' => $p_fecha_reg);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function engNumberDecimals(int $p_id): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "select number_decimals from base_currency where id=:p_id";
                   
        $param = array(':p_id' => $p_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function engGetObligation(int $p_proveedor, string $p_documento, int $p_tdoc): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "select a.*,
                (select identifier from rh_partner where id=a.partner_id) as rut,
                (select identifier_dv from rh_partner where id=a.partner_id) as dig,
                (select name from rh_partner where id=a.partner_id) as proveedor,
                (select name from base_currency where id=a.current_id) as moneda,
                (select name from base_document_type where id=a.document_type_id) as name_document_type,
                (select name from base_cost_center where id=a.cost_center_id) as name_cost_center,
                (select cost_center_code from base_cost_center where id=a.cost_center_id) as code_cost_center
                from aps_document a
                where a.document_type_id=:p_tdoc
                and a.documento=UPPER(:p_documento)
                and a.partner_id=:p_proveedor
                and a.status='C' ";
                   
        $param = array(':p_proveedor' => $p_proveedor,':p_documento' => $p_documento,':p_tdoc' => $p_tdoc);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function engvalidateSupplierObligation(string $p_rut, int $p_tdoc, string $p_documento): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "select count(*) as validate from aps_document 
                    where partner_id=(select id from rh_partner where identifier=:p_rut) 
                        and document_type_id=:p_tdoc 
                            and documento=:p_documento
                                and status='C' ";
                   
        $param = array(':p_rut' => $p_rut, ':p_tdoc' => $p_tdoc, ':p_documento' => $p_documento);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

    public function engAnulationObligation(int $p_id): array{
        $dataBase = new DataBaseKlinik($this->logger);        
        
        $sql = "update aps_document set status='R' where id=:p_id";
                   
        $param = array(':p_id' => $p_id);
        $data = $dataBase->execQueryParam("SELECT",$sql,$param);
        return $data;      
    }

}
