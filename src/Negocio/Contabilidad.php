<?php
declare(strict_types=1);

namespace App\Negocio;

use App\Datos\Contabilidad\ContabilidadSql;
use App\Datos\Engine\EngineSql;
use Psr\Log\LoggerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Contabilidad{
    protected $company_id;
    protected $logger;

    public function __construct(array $p_data,LoggerInterface $logger){
        $this->company_id = (int) $p_data["company_id"];
        $this->logger = $logger;
    }

    public function accEntryCodeCompanyYear(int $p_entry_year): int{
        
        $contabilidad = new ContabilidadSql($this->logger);
        $entry_code = $contabilidad->accEntryCodeCompanyYear($this->company_id,$p_entry_year);

        return $entry_code;
    }

    public function accPeriodIsOpen(string $p_date): bool{        
        $contabilidad = new ContabilidadSql($this->logger);
        $data = $contabilidad->accPeriodOpenForDate($this->company_id,$p_date);
        if(count($data)>0){
            return true;
        }
        else{
            return false;
        }
    }

    public function accVoucherTypeListAll(): array{  
        $errores = array();
        $mensajes = array();
        $data = array();

        $contabilidad = new ContabilidadSql($this->logger);
        $result = $contabilidad->accVoucherTypeListAll($this->company_id);
        return Response::response("ok",$errores,$mensajes,$result);
    }

    public function accAccountListAll(string $p_name): array{  
        $errores = array();
        $mensajes = array();
        $data = array();

        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accAccountListAll($this->company_id,$p_name);
        return Response::response("ok",$errores,$mensajes,$dataset);
    }

    public function accAccountListTree(string $p_name): array{  
        $errores = array();
        $mensajes = array();
        $data = array();

        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accAccountListAll($this->company_id,$p_name);
        $result = $this->accAccountCreateTree($dataset,0);
        return Response::response("ok",$errores,$mensajes,$result);
    }

    public function accAccountsListChargeable(): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accAccountsListChargeable();
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accAccountCreateTree(array $dataset,int $p_parent_id): array{
        $row = 0;
        $data = array();
        for($i=0;$i<count($dataset);$i++){
            if(empty($dataset[$i]['parent_id']) && $p_parent_id==0){
                $data[$row] = $dataset[$i];
                //$data[$row]['text'] = substr($dataset[$i]['name'],0,80);
                $data[$row]['text'] = $dataset[$i]['name'];
                $data[$row]['text_large'] = $dataset[$i]['name'];
                $data[$row]['children'] = $this->accAccountCreateTree($dataset,$dataset[$i]['id']);
                $row++;
            }
            else if($dataset[$i]['parent_id']==$p_parent_id){
                $data[$row] = $dataset[$i];
                //$data[$row]['text'] = substr($dataset[$i]['name'],0,80);
                $data[$row]['text'] = $dataset[$i]['name'];
                $data[$row]['text_large'] = $dataset[$i]['name'];

                if($dataset[$i]['type_name']=='group'){
                    $data[$row]['children'] = $this->accAccountCreateTree($dataset,$dataset[$i]['id']);
                    if(count($data[$row]['children'])==0){
                        $data[$row]['children'] = array(array('id'=>0, 'text'=>'Ninguna', 'isLeaf'=>true));
                    }
                }                    
                else
                    $data[$row]['isLeaf'] = true;

                $row++;
            }
        }
        return $data;
    }

    public function accEntryView(array $p_request): array{        
        $errores = array();
        $mensajes = array();
        $data = array();

        $entry_id = $p_request["var1"];

        if(empty($entry_id)){
            $errores[] = "Asiento no válido.";
        }
        
        if(count($errores)==0){
            $contabilidad = new ContabilidadSql($this->logger);
            $engine = new EngineSql($this->logger);
            
            $entry = $contabilidad->accEntryView($entry_id);
            if(count($entry)==0){
                $data["existe"] = false;
                $errores[] = "No se encontró el asiento";
            }
            else{
                $data["existe"] = true;
                $data["entry"] = $entry[0];
                $data["isOpenPeriod"] = $this->accPeriodIsOpen($data["entry"]["entry_date"]);
                $vouchertype = $contabilidad->accVoucherTypeView($entry[0]["voucher_type_id"]);
                $data["voucher_type"] = $vouchertype[0];
                $company = $engine->baseCompanySelect($data["entry"]["company_id"]);
                $data["company"] = $company[0];

                $entry_detail = $contabilidad->accEntryDetailView($entry[0]["id"]);
                if(count($entry_detail)==0){
                    $errores[] = "No se encontró el detalle del asiento";
                }
                else{
                    for($i=0;$i<count($entry_detail);$i++){
                        $account = $contabilidad->accAccountView($entry_detail[$i]["account_id"]);
                        $entry_detail[$i]["account"] =  $account[0];
                        $entry_detail[$i]["partner"] =  array("id" => 0, "name" => "", "identifier" => "", "identifier_dv" => "");
                        if(!empty($entry_detail[$i]["partner_id"])){
                            $partner = $engine->rhPartnerSelect($entry_detail[$i]["partner_id"]);
                            $entry_detail[$i]["partner"] =  $partner[0];
                        }
                    }

                    $data["entry_detail"] = $entry_detail;
                }
            }
        }


        if(count($errores)>0) $estado = "error";
        else $estado = "ok";

        return Response::response($estado,$errores,$mensajes,$data);
    }

    public function accEntryGet(array $p_request): array{        
        $errores = array();
        $mensajes = array();
        $data = array();

        $entry_code = $p_request["var1"];
        $entry_year = $p_request["var2"];

        if(empty($entry_code)){
            $errores[] = "Debe ingreso el número de asiento.";
        }
        if(empty($entry_year)){
            $errores[] = "El año no es válido.";
        }
        if(count($errores)==0){
            $contabilidad = new ContabilidadSql($this->logger);
            $engine = new EngineSql($this->logger);
            
            $entry = $contabilidad->accEntryViewForCompanyCodeYear($this->company_id, $entry_code, $entry_year);
            if(count($entry)==0){
                $data["existe"] = false;
                $errores[] = "No se encontró el asiento";
            }
            else{
                $data["existe"] = true;
                $data["entry"] = $entry[0];
                $data["isOpenPeriod"] = $this->accPeriodIsOpen($data["entry"]["entry_date"]);
                $vouchertype = $contabilidad->accVoucherTypeView($entry[0]["voucher_type_id"]);
                $data["voucher_type"] = $vouchertype[0];

                $entry_detail = $contabilidad->accEntryDetailView($entry[0]["id"]);
                if(count($entry_detail)==0){
                    $errores[] = "No se encontró el detalle del asiento";
                }
                else{
                    for($i=0;$i<count($entry_detail);$i++){
                        $account = $contabilidad->accAccountView($entry_detail[$i]["account_id"]);
                        $entry_detail[$i]["account"] =  $account[0];
                        $entry_detail[$i]["partner"] =  array("id" => 0, "name" => "", "identifier" => "", "identifier_dv" => "");
                        if(!empty($entry_detail[$i]["partner_id"])){
                            $partner = $engine->rhPartnerSelect($entry_detail[$i]["partner_id"]);
                            $entry_detail[$i]["partner"] =  $partner[0];
                        }
                    }

                    $data["entry_detail"] = $entry_detail;
                }
            }
        }


        if(count($errores)>0) $estado = "error";
        else $estado = "ok";

        return Response::response($estado,$errores,$mensajes,$data);
    }

    public function accEntrySave(array $p_request,string $p_user): array{        
        $errores = array();
        $mensajes = array();
        $data = array();

        $entry = $p_request["general"];
        $entry_detail = $p_request["detalle"];
        $vouchertype = $p_request["vouchertype"];

        $contabilidad = new ContabilidadSql($this->logger);
        $engine = new EngineSql($this->logger);

        $base_user = $engine->baseUserForUserName($p_user);

        //validar datos
        if(empty($entry["company_id"])){
            $errores[] = "Valor no válido para la empresa.";
        }
        if(empty($entry["entry_date"])){
            $errores[] = "Valor no válido para la fecha.";
        }
        else{
            if(!$this->accPeriodIsOpen($entry["entry_date"])){
                $errores[] = "La Fecha ingresa se encuentra en un periodo cerrado.";    
            }
        }
        if(empty($entry["description"])){
            $errores[] = "Valor no válido para la glosa.";
        }
        if(empty($entry["voucher_type_code"])){
            $errores[] = "Valor no válido para el tipo de comprobante.";
        }
        if(count($base_user)==0){
            $errores[] = "Su sesión ha caducado. Inicie sesión nuevamente.";
        }
        else{
            $entry["create_uid"] = $base_user[0]["id"];
        }

        $total_debit = 0;
        $total_credit = 0;
        if(count($entry_detail)<2){
            $errores[] = "El registro contable debe contener al menos dos líneas.";
        }
        else{
            try{
                for($i=0;$i<count($entry_detail);$i++){
                    $debit = (int) $entry_detail[$i]["debit"];
                    $credit = (int) $entry_detail[$i]["credit"];

                    if($debit!=0 || $credit!=0){
                        $total_debit += $debit;
                        $total_credit += $credit;
                    }
                    else{
                        $errores[] = "Existe un error en el detalle.";
                    }
                }

                if($total_debit!=$total_credit){
                    $errores[] = "El asiento se encuentra descuadrado.";
                }
            }
            catch(\Exception $e) {
                $errores[] = "Existe un error en el detalle.";
            }
        }

        if(count($errores)==0){
            if(empty($entry["id"])){
                $entry["id"] = $contabilidad->accEntryIdSeq();
                $entry["entry_code"] = $contabilidad->accEntryCodeCompanyYear($entry["company_id"],$entry["entry_year"]);
    
                $param = array(':p_id' => $entry["id"], ':p_company_id' => $entry["company_id"], ':p_entry_code' => $entry["entry_code"]
                            , ':p_entry_year' => $entry["entry_year"], ':p_entry_month' => $entry["entry_month"], ':p_entry_date' => $entry["entry_date"]
                            , ':p_description' => $entry["description"]
                            , ':p_debit' => $total_debit, ':p_credit' => $total_credit, ':p_voucher_type_id' => $vouchertype["id"]
                            , ':p_create_uid' => $entry["create_uid"], ':p_create_module' => $entry["create_module"]);

                if(!$contabilidad->accEntryInsert($param)){
                    $errores[] = "Ocurrió un error al realizar la inserción.";
                }
            }
            else{
                $param = array(':p_id' => $entry["id"], ':p_entry_year' => $entry["entry_year"], ':p_entry_month' => $entry["entry_month"], ':p_entry_date' => $entry["entry_date"]
                        , ':p_description' => $entry["description"], ':p_debit' => $entry["debit"], ':p_credit' => $entry["credit"], ':p_voucher_type_id' => $entry["voucher_type_id"]);
    
                if(!$contabilidad->accEntryUpdate($param)){
                    $errores[] = "Ocurrió un error al realizar la actualización.";
                }
                if(!$contabilidad->accEntryDetailDeteleAll($entry["id"])){
                    $errores[] = "Ocurrió un error al eliminar el detalle.";
                }
            }
    
            if(count($errores)==0){
                for($i=0;$i<count($entry_detail);$i++){
                    $debit = (int) $entry_detail[$i]["debit"];
                    $credit = (int) $entry_detail[$i]["credit"];

                    $param = array(':p_entry_id' => $entry["id"], ':p_line' => $entry_detail[$i]["line"], ':p_account_id' => $entry_detail[$i]["id"]
                            , ':p_debit' => $debit, ':p_credit' => $credit
                            , ':p_description' => $entry_detail[$i]["detalle"], ':p_cost_center_id' => null, ':p_partner_id' => $entry_detail[$i]["partner_id"]
                            , ':p_initial_document_id' => null, ':p_current_document_id' => null);
                                
                
                    if(!$contabilidad->accEntryDetailSave($param)){
                        $errores[] = "Ocurrió un error al guardar el detalle.";
                    }
                }
            }
        }

        if(count($errores)>0) $estado = "error";
        else $estado = "ok";

        return Response::response($estado,$errores,$mensajes,$entry);
    }

    public function accEntryDelete(array $p_request,string $p_user): array{        
        $errores = array();
        $mensajes = array();
        $data = array();

        $entry = $p_request["general"];
        $entry_detail = $p_request["detalle"];
        $vouchertype = $p_request["vouchertype"];

        $contabilidad = new ContabilidadSql($this->logger);
        $engine = new EngineSql($this->logger);

        $base_user = $engine->baseUserForUserName($p_user);

        //validar datos
        if(empty($entry["id"])){
            $errores[] = "No ha seleccionado ningún asiento contable.";
        }
        if(empty($entry["entry_date"])){
            $errores[] = "Valor no válido para la fecha.";
        }
        else{
            if(!$this->accPeriodIsOpen($entry["entry_date"])){
                $errores[] = "No es posible eliminar el asiento, ya que fue generado en un periodo que se encuentra cerrado.";    
            }
        }

        if(count($errores)==0){
            if(!$contabilidad->accEntryDelete($entry["id"],$base_user[0]["id"])){
                $errores[] = "Ocurrió un error al realizar la eliminación.";
            }
        }

        if(count($errores)>0) $estado = "error";
        else $estado = "ok";

        return Response::response($estado,$errores,$mensajes,$entry);
    }

    public function accEntryImportDetail(array $p_files): array{   
        $errores = array();
        $mensajes = array();
        $data = array();

        $file = $p_files["fileEntryDetail"];
        if(empty($file)){
            $errores[] = "No existe el archivo";
        }
        $name = $file["name"];
        $rutaArchivo = $file["tmp_name"];
        $info = new \SplFileInfo($name);
        $extension = $info->getExtension();
        
        if(count($errores)==0){
            $correcto = in_array($extension,array("xls","xlsx"));
            if($correcto){
                $index = 0;
                $contabilidad = new ContabilidadSql($this->logger);
                $engine = new EngineSql($this->logger);
                
                $documento = IOFactory::load($rutaArchivo);
                $totalDeHojas = $documento->getSheetCount();
                $hojaActual = $documento->getActiveSheet();
                $highestRow = $hojaActual->getHighestRow();
                $highestColumn = $hojaActual->getHighestColumn();
            
                $lines = $highestRow - 1;
                if ($lines <= 0) {
                    $errores[] = "No se encontraron datos en el archivo.";
                }
                
                $total_debito = 0;
                $total_credito = 0;
                for ($row = 1; $row <= $highestRow; ++$row) {
                    $code = $hojaActual->getCellByColumnAndRow(1, $row)->getValue();
                    $account = $contabilidad->accAccountViewForCode($this->company_id,$code);
                    $id = -1;
                    $name = "";
                    if(count($account)==0){
                        $errores[] = "Línea $row: La cuenta $code no existe.";
                    }
                    else{
                        $id = $account[0]["id"];
                        $name = $account[0]["name"];
                    }
    
                    $debit = $hojaActual->getCellByColumnAndRow(2, $row)->getValue();
                    $credit = $hojaActual->getCellByColumnAndRow(3, $row)->getValue();
                    $rut = (string) $hojaActual->getCellByColumnAndRow(4, $row)->getValue();
                    $detalle = $hojaActual->getCellByColumnAndRow(5, $row)->getValue();
                    if(empty($debit)) $debit = 0;
                    if(empty($credit)) $credit = 0;
                    if(empty($rut)) $rut = "";
                    if(empty($detalle)) $detalle = "";

                    //Validar rut
                    $nombre = "";
                    if($rut!=""){
                        //buscar si viene con -
                        $pos = strpos($rut, "-");
                        $rut2 = $rut;
                        if(!($pos===false)){
                            $rut = substr($rut,0,$pos);
                        }
                        
                        $partner = $engine->rhPartnerSelectForIdentifier($rut);
                        if(count($partner)==0){
                            $errores[] = "Línea $row: El RUT $rut2 $rut no existe.";
                        }
                        else{
                            $nombre = $partner[0]["name"];
                            $partner_id = $partner[0]["id"];
                        }
                    }

                    if($debit<>0){
                        $data[$index] = array("line" => ($index+1)
                                            , "id" => $id, "code" => $code, "name" => $name
                                            , "debit" => $debit, "credit" => 0
                                            , "rut" => $rut, "detalle" => $detalle
                                            , "nombre" => $nombre, "partner_id" => $partner_id
                                        );
                        $index++;
                    }
            
                    if($credit<>0){
                        $data[$index] = array("line" => ($index+1)
                                            , "id" => $id, "code" => $code, "name" => $name
                                            , "debit" => 0, "credit" => $credit
                                            , "rut" => $rut, "detalle" => $detalle
                                            , "nombre" => $nombre, "partner_id" => $partner_id
                                        );
                        $index++;
                    }

                    $total_debito += $debit;
                    $total_credito += $credit;
                }

                if($total_debito<>$total_credito){
                    $errores[] = "El monto total al debe y al haber son distintos.";
                }
            }
        }

        $status = "ok";
        if(count($errores)>0) $status="error";
        return Response::response($status,$errores,$mensajes,$data);
    }

    public function accPagaresNoContabilizados(array $p_request): array{
        $errores = array();
        $mensajes = array();
        $data = array();

        $finanzas = new Finanzas($this->logger);
        $pagares = $finanzas->buscarPagaresNoNulos($p_request);
        $pag_data = $pagares["data"]["pagares"];
        $pag_status = $pagares["status"];
        $errores = $pagares["errores"];

        $data["existe"] = false;
        $data["pagares"] = array();
        //$this->logger->info("accPagaresNoContabilizados pagares", $pagares);
        if($pag_status=="ok" && count($pag_data)>0){
            $data["existe"] = true;
            //Validar si el pagaré no está contabilizado
            $contabilidad = new ContabilidadSql($this->logger);
            foreach($pag_data as $row){
                $in_pagares = $contabilidad->accInPagaresDetailForIpgCod($row["ipg_cod"]);
                if(count($in_pagares)==0){                    
                    $row["entry_id"] = "";
                    $row["entry_code"] = "";
                    $row["contabilizado"] = false;
                    $row["contabilizar"] = false;
                }
                else{
                    $row["entry_id"] = $in_pagares[0]["entry_id"];
                    $row["entry_code"] = $in_pagares[0]["entry_code"];
                    $row["contabilizado"] = true;
                    $row["contabilizar"] = true;
                }
                $data["pagares"][] = $row;
            }
        }

        $status = "ok";
        if(count($errores)>0) $status="error";
        return Response::response($status,$errores,$mensajes,$data);
    }

    public function accConveniosNoContabilizados(array $p_request): array{
        $errores = array();
        $mensajes = array();
        $data = array();

        $finanzas = new Finanzas($this->logger);
        $convenios = $finanzas->buscarConveniosNoNulos($p_request);
        $pag_data = $convenios["data"]["convenios"];
        $pag_status = $convenios["status"];
        $errores = $convenios["errores"];

        $data["existe"] = false;
        $data["convenios"] = array();
        //$this->logger->info("accPagaresNoContabilizados convenios", $convenios);
        if($pag_status=="ok" && count($pag_data)>0){
            $data["existe"] = true;
            //Validar si el pagaré no está contabilizado
            $contabilidad = new ContabilidadSql($this->logger);
            foreach($pag_data as $row){
                $in_convenios = $contabilidad->accInConveniosDetailForIcCod($row["ic_cod"]);
                if(count($in_convenios)==0){                    
                    $row["entry_id"] = "";
                    $row["entry_code"] = "";
                    $row["contabilizado"] = false;
                    $row["contabilizar"] = false;
                }
                else{
                    $row["entry_id"] = $in_convenios[0]["entry_id"];
                    $row["entry_code"] = $in_convenios[0]["entry_code"];
                    $row["contabilizado"] = true;
                    $row["contabilizar"] = true;
                }
                $data["convenios"][] = $row;
            }
        }

        $status = "ok";
        if(count($errores)>0) $status="error";
        return Response::response($status,$errores,$mensajes,$data);
    }

    public function accPagaresContabilizar(array $p_request,string $p_user): array{
        $errores = array();
        $mensajes = array();
        $data = array();

        $general = $p_request["general"];
        $detalle = $p_request["detalle"];

        $contabilidad = new ContabilidadSql($this->logger);
        $engine = new EngineSql($this->logger);

        $base_user = $engine->baseUserForUserName($p_user);

        //validar datos
        if(empty($general["company_id"])){
            $errores[] = "Valor no válido para la empresa.";
        }
        if(empty($general["entry_date"])){
            $errores[] = "Valor no válido para la fecha.";
        }
        else{
            if(!$this->accPeriodIsOpen($general["entry_date"])){
                $errores[] = "La Fecha ingresa se encuentra en un periodo cerrado.";    
            }
        }
        if(count($base_user)==0){
            $errores[] = "Su sesión ha caducado. Inicie sesión nuevamente.";
        }
        else{
            $general["create_uid"] = $base_user[0]["id"];
        }

        if(count($errores)==0){
            $general["id"] = $contabilidad->accInPagaresIdSeq();
            $param = array(':p_id' => $general["id"], ':p_company_id' => $general["company_id"], ':p_create_uid' => $general["create_uid"]);

            if(!$contabilidad->accInPagaresInsert($param)){
                $errores[] = "Ocurrió un error al realizar la inserción de la nomina.";
            }
    
            if(count($errores)==0){
                for($i=0;$i<count($detalle);$i++){
                    if($detalle[$i]["contabilizar"]==true && $detalle[$i]["contabilizado"]==false){
                                                    
                        $entry_id = $contabilidad->accEntryIdSeq();
                        $entry_code = $contabilidad->accEntryCodeCompanyYear($general["company_id"],$general["entry_year"]);
                        $description = "PAGARE: ".$detalle[$i]["folio"]." PRESUPUESTO: ".$detalle[$i]["presupuestos"]." CUOTAS: ".$detalle[$i]["cuotas"];
                        $partner_id = 1;

                        $param = array(':p_id' => $entry_id, ':p_company_id' => $general["company_id"], ':p_entry_code' => $entry_code
                                    , ':p_entry_year' => $general["entry_year"], ':p_entry_month' => $general["entry_month"], ':p_entry_date' => $general["entry_date"]
                                    , ':p_description' => $description
                                    , ':p_debit' => $detalle[$i]["total"], ':p_credit' => $detalle[$i]["total"], ':p_voucher_type_id' => 1
                                    , ':p_create_uid' => $general["create_uid"], ':p_create_module' => $general["create_module"]);

                        if(!$contabilidad->accEntryInsert($param)){
                            $errores[] = "Ocurrió un error al realizar la inserción del encabezado del asiento.";
                        }
                        else{
                                    
                            $detalle[$i]["entry_id"] = $entry_id;
                            $detalle[$i]["entry_code"] = $entry_code;
                            $detalle[$i]["contabilizado"] = true;

                            $cuotas = $detalle[$i]["detalle_cuotas"];
                            $linea = 1;
                            foreach($cuotas as $cuota){
                                $description_cuota = "PAGARE: ".$detalle[$i]["folio"]." PRESUPUESTO: ".$detalle[$i]["presupuestos"]." CUOTA: ".$cuota["dtmv_cuota"];

                                $param = array(':p_in_pagares_id' => $general["id"], ':p_ipg_cod' => $detalle[$i]["folio"], ':p_ipg_cuota' => $cuota["dtmv_cuota"], ':p_ipg_valor' => $cuota["dtmv_valorpag"], ':p_entry_id' => $entry_id);                                
                            
                                if(!$contabilidad->accInPagaresDetailInsert($param)){
                                    $errores[] = "Ocurrió un error al guardar el detalle.";
                                }
                                else{
                                    $debit = (int) $cuota["dtmv_valorpag"];
                                    $credit = 0;
    
                                    $param = array(':p_entry_id' => $entry_id, ':p_line' => $linea, ':p_account_id' => 77
                                            , ':p_debit' => $debit, ':p_credit' => $credit
                                            , ':p_description' => $description_cuota, ':p_cost_center_id' => null, ':p_partner_id' => $partner_id
                                            , ':p_initial_document_id' => null, ':p_current_document_id' => $detalle[$i]["folio"]);                                            
                                
                                    if(!$contabilidad->accEntryDetailSave($param)){
                                        $errores[] = "Ocurrió un error al guardar el detalle de la cuota: ".$cuota["dtmv_cuota"];
                                    }     
    
                                    $linea++;
                                }
                            }

                            $debit = 0;
                            $credit = (int) $detalle[$i]["total"];

                            $param = array(':p_entry_id' => $entry_id, ':p_line' => $linea, ':p_account_id' => 226
                                    , ':p_debit' => $debit, ':p_credit' => $credit
                                    , ':p_description' => $description, ':p_cost_center_id' => null, ':p_partner_id' => $partner_id
                                    , ':p_initial_document_id' => null, ':p_current_document_id' => $detalle[$i]["folio"]);                                            
                        
                            if(!$contabilidad->accEntryDetailSave($param)){
                                $errores[] = "Ocurrió un error al guardar el detalle de la cuota: ".$cuota["dtmv_cuota"];
                            }

                            $linea++;                        
                        }
                    }
                }
            }
        }

        $data["general"] = $general;
        $data["detalle"] = $detalle;

        $status = "ok";
        if(count($errores)>0) $status="error";
        return Response::response($status,$errores,$mensajes,$data);
    }

    public function accConveniosContabilizar(array $p_request,string $p_user): array{
        $errores = array();
        $mensajes = array();
        $data = array();

        $general = $p_request["general"];
        $detalle = $p_request["detalle"];

        $contabilidad = new ContabilidadSql($this->logger);
        $engine = new EngineSql($this->logger);

        $base_user = $engine->baseUserForUserName($p_user);

        //validar datos
        if(empty($general["company_id"])){
            $errores[] = "Valor no válido para la empresa.";
        }
        if(empty($general["entry_date"])){
            $errores[] = "Valor no válido para la fecha.";
        }
        else{
            if(!$this->accPeriodIsOpen($general["entry_date"])){
                $errores[] = "La Fecha ingresa se encuentra en un periodo cerrado.";    
            }
        }
        if(count($base_user)==0){
            $errores[] = "Su sesión ha caducado. Inicie sesión nuevamente.";
        }
        else{
            $general["create_uid"] = $base_user[0]["id"];
        }

        if(count($errores)==0){
            $general["id"] = $contabilidad->accInConveniosIdSeq();
            $param = array(':p_id' => $general["id"], ':p_company_id' => $general["company_id"], ':p_create_uid' => $general["create_uid"]);

            if(!$contabilidad->accInConveniosInsert($param)){
                $errores[] = "Ocurrió un error al realizar la inserción de la nomina.";
            }
    
            if(count($errores)==0){
                for($i=0;$i<count($detalle);$i++){
                    if($detalle[$i]["contabilizar"]==true && $detalle[$i]["contabilizado"]==false){
                                                    
                        $entry_id = $contabilidad->accEntryIdSeq();
                        $entry_code = $contabilidad->accEntryCodeCompanyYear($general["company_id"],$general["entry_year"]);
                        $desc_pptos = "";
                        $desc_oes = "";
                        $desc_ods = "";
                        if(!empty($detalle[$i]["presupuestos"])) $desc_pptos = " PRESUPUESTO: ".$detalle[$i]["presupuestos"];
                        if(!empty($detalle[$i]["oexternas"])) $desc_oes = " O. CONSULTA: ".$detalle[$i]["oexternas"];
                        if(!empty($detalle[$i]["odentales"])) $desc_ods = " O. DENTAL: ".$detalle[$i]["odentales"];
                        
                        $description = "CONVENIO: ".$detalle[$i]["folio"].$desc_pptos.$desc_oes.$desc_ods." CUOTAS: ".$detalle[$i]["cuotas"];
                        $partner_id = 1;

                        $param = array(':p_id' => $entry_id, ':p_company_id' => $general["company_id"], ':p_entry_code' => $entry_code
                                    , ':p_entry_year' => $general["entry_year"], ':p_entry_month' => $general["entry_month"], ':p_entry_date' => $general["entry_date"]
                                    , ':p_description' => $description
                                    , ':p_debit' => $detalle[$i]["total"], ':p_credit' => $detalle[$i]["total"], ':p_voucher_type_id' => 1
                                    , ':p_create_uid' => $general["create_uid"], ':p_create_module' => $general["create_module"]);

                        if(!$contabilidad->accEntryInsert($param)){
                            $errores[] = "Ocurrió un error al realizar la inserción del encabezado del asiento.";
                        }
                        else{
                                    
                            $detalle[$i]["entry_id"] = $entry_id;
                            $detalle[$i]["entry_code"] = $entry_code;
                            $detalle[$i]["contabilizado"] = true;

                            $cuotas = $detalle[$i]["detalle_cuotas"];
                            $linea = 1;
                            foreach($cuotas as $cuota){
                                $description_cuota = "CONVENIO: ".$detalle[$i]["folio"].$desc_pptos.$desc_oes.$desc_ods." CUOTA: ".$cuota["dtmv_cuota"];

                                $param = array(':p_in_convenios_id' => $general["id"], ':p_ic_cod' => $detalle[$i]["folio"], ':p_ic_cuota' => $cuota["dtmv_cuota"], ':p_ic_valor' => $cuota["dtmv_valor"], ':p_entry_id' => $entry_id);                                
                            
                                if(!$contabilidad->accInConveniosDetailInsert($param)){
                                    $errores[] = "Ocurrió un error al guardar el detalle.";
                                }
                                else{
                                    $debit = (int) $cuota["dtmv_valor"];
                                    $credit = 0;
    
                                    $param = array(':p_entry_id' => $entry_id, ':p_line' => $linea, ':p_account_id' => 94
                                            , ':p_debit' => $debit, ':p_credit' => $credit
                                            , ':p_description' => $description_cuota, ':p_cost_center_id' => null, ':p_partner_id' => $partner_id
                                            , ':p_initial_document_id' => null, ':p_current_document_id' => $detalle[$i]["folio"]);                                            
                                
                                    if(!$contabilidad->accEntryDetailSave($param)){
                                        $errores[] = "Ocurrió un error al guardar el detalle de la cuota: ".$cuota["dtmv_cuota"];
                                    }     
    
                                    $linea++;
                                }
                            }

                            $debit = 0;
                            $credit = (int) $detalle[$i]["total"];

                            $param = array(':p_entry_id' => $entry_id, ':p_line' => $linea, ':p_account_id' => 228
                                    , ':p_debit' => $debit, ':p_credit' => $credit
                                    , ':p_description' => $description, ':p_cost_center_id' => null, ':p_partner_id' => $partner_id
                                    , ':p_initial_document_id' => null, ':p_current_document_id' => $detalle[$i]["folio"]);                                            
                        
                            if(!$contabilidad->accEntryDetailSave($param)){
                                $errores[] = "Ocurrió un error al guardar el detalle de la cuota: ".$cuota["dtmv_cuota"];
                            }

                            $linea++;                        
                        }
                    }
                }
            }
        }

        $data["general"] = $general;
        $data["detalle"] = $detalle;

        $status = "ok";
        if(count($errores)>0) $status="error";
        return Response::response($status,$errores,$mensajes,$data);
    }

    public function accValDateReportBookBig(string $p_fechaini, string $p_fechafin): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accValDateReportBookBig($p_fechaini, $p_fechafin);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accValAccountReportBookBigDet(int $p_id, int $p_account): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accValAccountReportBookBigDet($p_id, $p_account);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accValPeriodReportBookBig(string $p_periodo): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accValPeriodReportBookBig($p_periodo);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accValReportDiaryBookDate(string $p_fechaini, string $p_fechafin): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accValReportDiaryBookDate($p_fechaini, $p_fechafin);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function accValReportDiaryBookPeriod(string $p_periodo): array{ 
        $contabilidad = new ContabilidadSql($this->logger);
        $dataset = $contabilidad->accValReportDiaryBookPeriod($p_periodo);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    private function response(string $p_header, string $p_status, string $p_message, array $p_data): array{
        return array("header" => $p_header, "status" => $p_status, "message" => $p_message, "data" => $p_data);
    }

    /*
    public function accVoucherTypeView(array $dataset): array{ 
        $result = $dataset;
        return $result;
    }

    public function accVoucherTypeUpdate(array $originData, array $formData): array{
        $id = $formData['key1'];
        $data["company_id"] = $formData['key2'];
        $data["voucher_type_code"] = $formData['key3'];
        $data["name"] = $formData['key4'];
        $data["description"] = $formData['key5'];
        $data["class_type"] = $formData['key6'];

        if (!isset($id) || empty($id) || count($originData)==0) {
            throw new ContabilidadNotFoundException();
        }

        $dataset = $originData;
        
        if($originData['company_id']!=$data["company_id"]){
            $dataset['company_id'] = $data["company_id"];
        }        
        if($originData['voucher_type_code']!=$data["voucher_type_code"]){
            $dataset['voucher_type_code'] = $data["voucher_type_code"];
        }        
        if($originData['name']!=$data["name"]){
            $dataset['name'] = $data["name"];
        }        
        if($originData['description']!=$data["description"]){
            $dataset['description'] = $data["description"];
        }        
        if($originData['class_type']!=$data["class_type"]){
            $dataset['class_type'] = $data["class_type"];
        }
        
        return  array($dataset['id'], $dataset['company_id'], $dataset['voucher_type_code'], $dataset['name'], $dataset['description'], $dataset['class_type']);
    }

    public function jsonSerialize()
    {
        return [
            
        ];
    }
    */   
}