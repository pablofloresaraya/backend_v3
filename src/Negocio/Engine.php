<?php
declare(strict_types=1);

namespace App\Negocio;

use App\Datos\Engine\EngineSql;
use Psr\Log\LoggerInterface;

class Engine{
    protected $company_id;
    protected $logger;

    public function __construct(array $p_data,LoggerInterface $logger){
        if(!empty($p_data["company_id"]))
            $this->company_id = (int) $p_data["company_id"];
            
        $this->logger = $logger;
    }

    public function getModules(string $p_type): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->getModules($p_type);
        $result = $dataset;
        return $result;
    } 

    public function getModuleAll(int $p_module): array{ 
        $result = array();
        if($p_module==-1){
            return $this->response("","error","modulo no valido",$result);
        }
        $engine = new EngineSql($this->logger);
        $dataset = $engine->getModuleAll($p_module);
        $result = $dataset;
        return $this->response("","ok","",$result);
    } 

    public function getModuleSections(int $p_module): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->getModuleSections($p_module);
        $result = $dataset;
        return $result;
    } 

    public function getModuleSectionMenu(int $p_module, int $p_section): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->getModuleSectionMenu($p_module, $p_section);
        $result = $dataset;
        return $result;
    } 

    public function baseUserLogin(string $p_username,string $p_password): array{
        $errores = array();
        $mensajes = array();
        $result = array();

        $engine = new EngineSql($this->logger);
        $user = $engine->baseUserLogin($p_username,$p_password);
        if(count($user)>0){
            if($user[0]["activo"]==true){
                $user_company = $engine->baseUserCompanySelectAllForUser($user[0]["id"]);
                $user_company_default = $engine->baseUserCompanyDefaultForUser($user[0]["id"]);
                $result = array("user" => $p_username, "name" => $user[0]["name"], "company_list" => $user_company, "company_default" => $user_company_default, "company_used" => $user_company_default[0]["company_id"]);
            }
            else{
                $errores[] = "El usuario no se encuentra activo";
            }
        }
        else{
            $errores[] = "Usuario o contraseña no validos";
        }

        if(count($errores)>0) $estado = "error";
        else $estado = "ok";

        return Response::response($estado,$errores,$mensajes,$result);
    }

    public function rhPartnerViewForRut(string $p_rut): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->rhPartnerViewForRut($p_rut);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function rhPartnerListCustormesSuppliers(): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->rhPartnerListCustormesSuppliers($this->company_id);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function rhPartnerIsSupplier(int $p_id): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->rhPartnerIsSupplier($p_id);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function rhPartnerIsCustomer(int $p_id): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->rhPartnerIsCustomer($p_id);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function rhPartnerSupplierDelete(int $p_id): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->rhPartnerSupplierDelete($p_id);
        $result = ['resp' => $dataset];
        return $this->response("","ok","",$result);
    }

    public function rhPartnerCustomerDelete(int $p_id): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->rhPartnerCustomerDelete($p_id);
        $result = ['resp' => $dataset];
        return $this->response("","ok","",$result);
    }

    public function rhPartnerSelectForIdentifier(string $p_identifier){
        $errores = array();
        $mensajes = array();
        $result = array();

        $engine = new EngineSql($this->logger);
        
        if($p_identifier!=""){
            //buscar si viene con -
            $pos = strpos($p_identifier, "-");
            $p_identifier2 = $p_identifier;
            if(!($pos===false)){
                $p_identifier = substr($p_identifier,0,$pos);
            }
            
            $partner = $engine->rhPartnerSelectForIdentifier($p_identifier);
            if(count($partner)==0){
                $errores[] = "El RUT $p_identifier2 no existe.";
            }
            else{
                $result = $partner[0];
            }         
        }
        else{
            $errores[] = "Valor no válido para el RUT.";
        }

        if(count($errores)>0) $estado = "error";
        else $estado = "ok";
        return Response::response($estado,$errores,$mensajes,$result);

    }
    
    public function rhPartnerIdSeq(){
        $engine = new EngineSql($this->logger);
        return $engine->rhPartnerIdSeq();
    }

    public function rhPartnerSave(int $p_id,string $p_identifier,string $p_dv,string $p_name,int $p_usuario,                                int $p_city,                               string $p_direccion,string $p_phone,string $p_mobile,string $p_email,string $p_giro,string $p_sucursal,string $p_sucursal_email ): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->rhPartnerSave($p_id,$p_identifier,$p_dv,$p_name,$p_usuario,                                $p_city,                               $p_direccion,$p_phone,$p_mobile,$p_email,$p_giro,$p_sucursal,$p_sucursal_email);
        $result = ["resp" => $dataset];
        return $this->response("","ok","",$result);
    }

    public function rhPartnerSupplierInsert(int $p_partner): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->rhPartnerSupplierInsert($p_partner);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function rhPartnerCustomerInsert(int $p_partner): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->rhPartnerCustomerInsert($p_partner);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function rhPartnerUpdate(string $p_identifier,string $p_identifier_dv,                                   string $p_name,int $p_usuario,int $p_city,string $p_direccion,string $p_phone,string $p_mobile,string $p_email,string $p_giro,string $p_sucursal,string $p_sucursal_email,int $p_id): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->rhPartnerUpdate(
                                                $p_identifier,
                                                $p_identifier_dv,                                   
                                                $p_name,
                                                $p_usuario,
                                                $p_city,
                                                $p_direccion,
                                                $p_phone,
                                                $p_mobile,
                                                $p_email,
                                                $p_giro,
                                                $p_sucursal,
                                                $p_sucursal_email,
                                                $p_id
                                            );
        $result = ["resp" => $dataset];
        return $this->response("","ok","",$result);
    }

    public function rhPartnerSupplierUpdate(int $p_id, string $p_proveedor): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->rhPartnerSupplierUpdate($p_id, $p_proveedor);
        $result = ["resp" => $dataset];
        return $this->response("","ok","",$result);
    }

    public function rhPartnerCustomerUpdate(int $p_id, string $p_cliente): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->rhPartnerCustomerUpdate($p_id, $p_cliente);
        $result = ["resp" => $dataset];
        return $this->response("","ok","",$result);
    }

    public function baseTerritorialRegionsListAll(): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->baseTerritorialRegionsListAll($this->company_id);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function baseTerritorialProvincesForRegion(int $p_region): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->baseTerritorialProvincesForRegion($p_region);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function baseTerritorialCommunesForProvince(int $p_provincia): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->baseTerritorialCommunesForProvince($p_provincia);
        $result = $dataset;
        return $this->response("","ok","",$result);
    }

    public function engBaseCurrencyView(): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->engBaseCurrencyView();
        
        $data = array();

        for($i=0;$i<count($dataset);$i++){
            $data[$i]['id']        = $dataset[$i]['id'];
            $data[$i]['nombre']    = $dataset[$i]['name'];
            $data[$i]['principal'] = $dataset[$i]['default_currency'] ? 'Si' : 'No';
        }

        return $this->response("","ok","",$data);
    }

    public function engBaseCurrencySave(array $p_request): array{
        
        $errores     = array();
        $mensajes    = array();
        $result      = array();
        
        //$company     = $p_request['company'];
        $name        = $p_request['nombre'];
        $description = $p_request['descripcion'];
        $value       = $p_request['valor'];

        if(empty($name)){
            $errores[] = "Valor no válido para el nombre.";
        }

        if(empty($value)){
            $errores[] = "Valor no válido para el tipo de valor.";
        }

        if(empty($description)){
            $errores[] = "Valor no válido para la descripción.";
        }

        if(count($errores)===0){
        
            $engine = new EngineSql($this->logger);
            $dataset = $engine->engBaseCurrencySave($name, $description, $value);
            $result = $dataset;           
        
        }

        if(count($errores)>0) $estado = "error";
        else $estado = "ok";

        return Response::response($estado,$errores,$mensajes,$result);
    }

    public function engBaseCurrencyDelete(array $p_request): array{ 
        $engine = new EngineSql($this->logger);
        
        $data = array();

        for($i=0;$i<count($p_request);$i++){
            $engine->engBaseCurrencyDelete($p_request[$i]);
        }

        return $this->response("","ok","",$data);
    }

    public function engBaseValueChangeView(): array{ 
        $engine = new EngineSql($this->logger);
        $dataset = $engine->engBaseValueChangeView();
        $data = array();

        for($i=0;$i<count($dataset);$i++){

            $fecha='';

            switch ($dataset[$i]['type_value']){
                case 1:
                    $fecha = $dataset[$i]['fecha']; //dd-mm-yyyy
                    break;
                case 2:
                    $fecha = $dataset[$i]['fecha_mes']; //mm-yyyy
                    break;
                case 3:
                    $fecha = $dataset[$i]['fecha_anio']; //yyyy
                    break;
            }            

            $data[$i]['id']    = $dataset[$i]['id'];
            $data[$i]['valor'] = $dataset[$i]['value'];
            $data[$i]['fecha'] = $fecha;

        }

        return $this->response("","ok","",$data);
    }

    public function engBaseValueChangeDelete(array $p_request): array{ 
        $engine = new EngineSql($this->logger);           
        $dataset=$engine->engBaseValueChangeDelete($p_request['id']);
        $result= ['result' => $dataset];
        return $this->response("","ok","",$result);
    }

    public function engBaseValueChangeSave(array $p_request): array{

        $errores     = array();
        $mensajes    = array();
        $result      = array();

        $tipo_moneda = $p_request['moneda'];
        $valor       = $p_request['datos']['valor'];
        $fecha       = $p_request['datos']['fecha'];
        
        if(empty($fecha)){
            $errores[] = "Valor no válido para la fecha.";
        }

        if(empty($valor)){
            $errores[] = "Valor no válido para el valor.";
        }

        if(empty($tipo_moneda)){
            $errores[] = "Valor no válido para el tipo de moneda.";
        }

        if(count($errores)===0){

            $dia  = date("d", strtotime($p_request['datos']['fecha']));
            $mes  = date("m", strtotime($p_request['datos']['fecha']));
            $anio = date("Y", strtotime($p_request['datos']['fecha']));
            
            $engine = new EngineSql($this->logger);           
            $dataset=$engine->engBaseValueChangeSave($p_request['moneda'], $p_request['datos']['valor'], $dia, $mes, $anio);
            $result=$dataset;

        }

        if(count($errores)>0) $estado = "error";
        else $estado = "ok";

        return Response::response($estado,$errores,$mensajes,$result);
    }

    public function rhPartnerListSupplier(): array{ 
        $engine = new EngineSql($this->logger);           
        $dataset=$engine->rhPartnerListSupplier();
        $result= $dataset;
        return $this->response("","ok","",$result);
    }

    public function engBaseDocumentType(): array{
        
        $engine = new EngineSql($this->logger);           
        $dataset=$engine->engBaseDocumentType();
        for($i=0;$i<count($dataset);$i++){
            $result[$i]['id'] = $dataset[$i]['id'];
            $result[$i]['nombre'] = $dataset[$i]['nombre']; 
            $result[$i]['codigo'] = $dataset[$i]['codigo']; 
            $result[$i]['impuesto'] = (!empty($dataset[$i]['impuesto']) ? $dataset[$i]['impuesto'] : 'N/A'); 
            $result[$i]['porcentaje'] = (!empty($dataset[$i]['porcentaje'])) ? $dataset[$i]['porcentaje'] : 0;  
        } 
        return $this->response("","ok","",$result);
    }

    public function engBaseCostCenter(): array{ 
        $engine = new EngineSql($this->logger);           
        $dataset=$engine->engBaseCostCenter();
        $result= $dataset;
        return $this->response("","ok","",$result);
    }

    public function engRegisterObligationsSave(array $p_request): array{

        $errores  = array();
        $mensajes = array();
        $result   = array();
        
        $company_id = $this->company_id;
        $proveedor = $p_request['rut']; // rut proveedor
        $partner_id = $p_request['partner'];
        $id_tipo_documento = $p_request['idtdoc'];
        $documento = $p_request['documento'];
        $fecha_documento = $p_request['fdocumento'];
        $fecha_registro = $p_request['fregistro'];
        $fecha_vencimiento = $p_request['fvencimiento'];
        $porcentaje = $p_request['porcentaje'];
        $id_moneda = $p_request['codmoneda'];
        $valor_cambio = $p_request['vcambio']; //valor cambio
        $p_amount_untaxed = null; //cantidad libre de impto.
        $impuesto = $p_request['impuesto'];
        $p_amount_exempt = null; //cantidad exenta
        $p_total = null;
        $p_balance_due = null; //saldo adeudado
        $p_amount_untaxed_local = null; //cantidad libre de impto local.
        $p_amount_tax_local = null; //caltidad impto. local
        $p_amount_exempt_local = null; //cantidad exenta local        
        $p_balance_due_local = null; //saldo adeudado local
        $descripcion = $p_request['descripcion'];
        $id_centro_costo = $p_request['ccosto']; 
        $entry_id = 1;
        $p_creation_user_id = 1;
        $p_creation_date = null;
        $p_monto_neto = null;

        $monto_exento = $p_request['exentoorigen'];
        $monto_neto   = $p_request['netoorigen'];
        $monto_impto  = $p_request['imptoorigen'];
        $total        = $p_request['totalorigen'];

        $monto_neto_local = $p_request['netolocal'];
        $monto_impto_local = $p_request['imptolocal'];
        $monto_exento_local = $p_request['exentolocal'];
        $total_local = $p_request['totallocal'];

        $engine = new EngineSql($this->logger);
        
        $validate=$engine->engvalidateSupplierObligation($proveedor, $id_tipo_documento, $documento);

        if($validate[0]['validate']===0){

            /*$tax_op=$engine->engTaxOperation($id_tipo_documento,$company_id);
            $porc_aux=$engine->engPorcentajeAux($id_tipo_documento,$fecha_registro);
            $num_decimals=$engine->engNumberDecimals($id_moneda);

            $tax_operation  = $tax_op[0]['tax_operation'];
            $porcentaje_aux = $porc_aux[0]['value'];
            $decimal_origen = $num_decimals[0]['number_decimals'];
            $decimal_local  = $num_decimals[0]['number_decimals'];

            $monto_exento = (empty($monto_exento)) ? 0 : $monto_exento;
            $monto_neto = (empty($monto_neto)) ? 0 : $monto_neto;
            $total = (empty($total)) ? 0 : $total;
            $monto_impto = (empty($monto_impto)) ? 0 : $monto_impto;

            if ($tax_operation === 'D'){
                $total = round($monto_neto * ((100 - $porcentaje_aux) / 100), $decimal_origen);
                $monto_impto = round($monto_neto-$total, $decimal_origen);
                $total = round($monto_neto - $monto_impto + $monto_exento,$decimal_origen);
            }
            else{
                $total = round($monto_neto + ($monto_neto * $porcentaje_aux / 100), $decimal_origen);
                $monto_impto = round($total - $monto_neto, $decimal_origen);
                $total = round($monto_neto + $monto_impto + $monto_exento, $decimal_origen);
            }

            //local
            if ($valor_cambio > 0){
                $monto_neto_local = round($monto_neto * $valor_cambio ,$decimal_local);
                $monto_impto_local = round($monto_impto * $valor_cambio ,$decimal_local);
                $monto_exento_local = round($monto_exento * $valor_cambio ,$decimal_local);
                $total_local = round($monto_neto_local - $monto_impto_local + $monto_exento_local,$decimal_local);
            }
            else{
                $monto_neto_local = $monto_neto;
                $monto_impto_local = $monto_impto;
                $monto_exento_local = $monto_exento;
                $total_local = $monto_neto_local + $monto_impto_local + $monto_exento_local;
            }*/
                    
            $dataset=$engine->engRegisterObligationsSave(
                (int) $company_id,
                (int) $partner_id,
                (int) $id_tipo_documento,
                (string) $documento,
                (string) $fecha_documento,
                (string) $fecha_registro,
                (string) $fecha_vencimiento,                
                (string) $porcentaje,
                (int)   $id_moneda,
                (float) $valor_cambio,
                (float) $monto_neto, //cantidad libre de impto. amount_untaxed
                (float) $monto_impto, //cantidad impto. origen
                (float) $monto_exento,//cantidad exenta - exenta origen
                (float) $total,
                (float) $total, //saldo adeudado p_balance_due
                (float) $monto_neto_local, //cantidad libre de impto local. amount_untaxed_local
                (float) $monto_impto_local, //cantidad impto. local
                (float) $monto_exento_local, //cantidad exenta local
                (float) $total_local,
                (float) $total_local, //saldo adeudado local p_balance_due_local
                (string) $descripcion,
                (int) $id_centro_costo, 
                (int) $entry_id,
                (int) $p_creation_user_id
                
            );

            $result = $dataset;

            $mensajes[] = "El proveedor se registro con éxito."; 

        }else{

            $errores[] = "El proveedor ya fue ingresado.";

        }

        if(count($errores)>0) $estado = "error";
        else $estado = "ok";

        return Response::response($estado,$errores,$mensajes,$result);
        
    }

    public function engGetObligation(array $p_request): array{

        $errores  = array();
        $mensajes = array();
        $result   = array();
        
        $partner   = $p_request['partner'];
        $proveedor = $p_request['proveedor']; //rut
        $idtdoc    = $p_request['idtdoc'];
        $documento = $p_request['documento'];

        if(!empty($partner) && !empty($idtdoc) && !empty($documento)){
        
            $engine = new EngineSql($this->logger);           
            $dataset=$engine->engGetObligation($partner,$documento,$idtdoc);

            if(count($dataset)>0){

                $array = array(
                    'id'            => $dataset[0]['id'],
                    'rut'           => $dataset[0]['rut'],
                    'dig'           => $dataset[0]['dig'],
                    'partner'       => $dataset[0]['partner_id'],
                    'proveedor'     => $dataset[0]['proveedor'],
                    'idtdoc'        => $dataset[0]['document_type_id'],
                    'tdoc'          => $dataset[0]['name_document_type'],
                    'impuesto'      => '',
                    'porcentaje'    => $dataset[0]['tax_percentage'],
                    'documento'     => $dataset[0]['documento'],
                    'fdocumento'    => $dataset[0]['document_date'],
                    'fvencimiento'  => $dataset[0]['expiration_date'],
                    'fregistro'     => $dataset[0]['accounting_date'],
                    'codmoneda'     => $dataset[0]['current_id'],
                    'moneda'        => $dataset[0]['moneda'],
                    'vcambio'       => $dataset[0]['exchange_value'],
                    'ccosto'        => $dataset[0]['cost_center_id'],
                    'codccosto'     => $dataset[0]['code_cost_center'],
                    'nccosto'       => $dataset[0]['name_cost_center'],
                    'descripcion'   => $dataset[0]['description'],
                    'netoorigen'    => $dataset[0]['amount_untaxed'],
                    'netolocal'     => $dataset[0]['amount_untaxed_local'],
                    'imptoorigen'   => $dataset[0]['amount_tax'],
                    'imptolocal'    => $dataset[0]['amount_tax_local'],
                    'exentoorigen'  => $dataset[0]['amount_exempt'],
                    'exentolocal'   => $dataset[0]['amount_exempt_local'],
                    'totalorigen'   => $dataset[0]['total'],
                    'totallocal'    => $dataset[0]['total_local'] 
                );

                $result= $array;

            }else{

                $errores[] = "No existe registro de Obligaciones";

            }

        }else{

            $errores[] = "No existe registro de Obligaciones";
        
        }

        if(count($errores)>0) $estado = "error";
        else $estado = "ok";
   
        return Response::response($estado,$errores,$mensajes,$result);
    }

    public function engAnulationObligation(array $p_request): array{
        $result   = array();
        $errores  = array(); 
        $mensajes = array(); 

        $engine = new EngineSql($this->logger);           
        $dataset=$engine->engAnulationObligation($p_request['id']);

        if(!$dataset) { 
            $erroes[] = "Error de Anulación"; 
        }else{ 
            $mensajes[] = "La Anulación se realizó con éxito"; 
        }

        if(count($errores)>0) $estado = "error";
        else $estado = "ok";

        return Response::response($estado,$errores,$mensajes,$result);
        
    }

    private function response(string $p_header, string $p_status, string $p_message, array $p_data): array{
        return array("header" => $p_header, "status" => $p_status, "message" => $p_message, "data" => $p_data);
    }
}