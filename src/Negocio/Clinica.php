<?php
declare(strict_types=1);

namespace App\Negocio;

use App\Datos\Clinica\ClinicaSql;
use Psr\Log\LoggerInterface;

class Clinica{
    protected $logger;

    public function __construct(LoggerInterface $logger){
        $this->logger = $logger;
    }

    public function getPaciente(array $p_request): array{       
        $errores = array();
        $mensajes = array();
        $data = array();

        $pc_cod = $p_request["var1"];
        $clinica = new ClinicaSql($this->logger);
        $result = $clinica->getPaciente($pc_cod);
        $data = $result[0];
        if(count($errores)>0) $estado = "error";
        else $estado = "ok";
        return Response::response($estado,$errores,$mensajes,$data);
    }

    public function getPresupuesto(array $p_request): array{       
        $errores = array();
        $mensajes = array();
        $data = array();

        $ps_cod = $p_request["var1"];
        $clinica = new ClinicaSql($this->logger);
        $result = $clinica->getPresupuesto($ps_cod);
        $data = $result[0];

        if(count($errores)>0) $estado = "error";
        else $estado = "ok";
        return Response::response($estado,$errores,$mensajes,$data);
    }

    public function getOExterna(array $p_request): array{       
        $errores = array();
        $mensajes = array();
        $data = array();

        $oe_cod = $p_request["var1"];
        $clinica = new ClinicaSql($this->logger);
        $result = $clinica->getOExterna($oe_cod);
        $data = $result[0];

        if(count($errores)>0) $estado = "error";
        else $estado = "ok";
        return Response::response($estado,$errores,$mensajes,$data);
    }

    public function getODental(array $p_request): array{       
        $errores = array();
        $mensajes = array();
        $data = array();

        $od_cod = $p_request["var1"];
        $clinica = new ClinicaSql($this->logger);
        $result = $clinica->getODental($od_cod);
        $data = $result[0];

        if(count($errores)>0) $estado = "error";
        else $estado = "ok";
        return Response::response($estado,$errores,$mensajes,$data);
    }
}