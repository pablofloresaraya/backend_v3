<?php
namespace App\Negocio;

use Psr\Log\LoggerInterface;
use Firebase\JWT\JWT;
use Dotenv\Dotenv;
use Tuupola\Base62;
use \DateTime;
use \DateTimeImmutable;

class Token{
    private $algorithm;
    private $secretkey;
    private $hrs_expired;
    private $servername;
    private $username;
    protected $logger;

    public function __construct($p_data, LoggerInterface $logger){
       $this->algorithm = $p_data["algorithm"];
       $this->secretkey = $p_data["secretkey"];
       $this->servername = $p_data["servername"];
       $this->username = $p_data["username"];
       $this->hrs_expired = $p_data["hrs_expired"];

       $this->logger = $logger;
    }

    public function encode(){
        $now = new DateTime();
        $future = new DateTime("now +".$this->hrs_expired." hours");
        $jti = (new Base62)->encode(random_bytes(16));
        $server_name = $this->servername;
        $username = $this->username;

        $payload = [
            "iat" => $now->getTimeStamp(),
            "exp" => $future->getTimeStamp(),
            "jti" => $jti,
            "iss" => $server_name,
            "username" => $username
            //"sub" => $server["PHP_AUTH_USER"],
            //"scope" => $scopes
        ];

        $token = JWT::encode($payload, $this->secretkey, $this->algorithm);
        
        return array("token" => $token, "expires" => $future->getTimeStamp());
    }

    public function decode($jwt){
        $token = JWT::decode($jwt, $this->secretkey, array($this->algorithm));
        return $token;
    }

    public function validate($header){
        $resultado = array("header" => "", "status" => "", "message" => "");
        if(empty($header["Authorization"])){
            $resultado["header"] = 'HTTP/1.0 400 Bad Request';
            $resultado["status"] = 'ERROR';
            $resultado["message"] = 'Header Authorization no existe';

            return $resultado;
        }
        
        if (! preg_match('/Bearer\s(\S+)/', $header["Authorization"], $matches)) {
            $resultado["header"] = 'HTTP/1.0 400 Bad Request';
            $resultado["status"] = 'ERROR';
            $resultado["message"] = 'Token no encontrado';

            return $resultado;
        }

        $jwt = $matches[1];
        if (!$jwt) {
            $resultado["header"] = 'HTTP/1.0 400 Bad Request';
            $resultado["status"] = 'ERROR';
            $resultado["message"] = 'El token no pudo ser extraido';

            return $resultado;
        }

        try {
            $this->logger->infor("jwt",$jwt);
            $token = $this->decode($jwt);        
        } 
        catch (\Exception $e) {
            $resultado["header"] = 'HTTP/1.1 401 Unauthorized';
            $resultado["status"] = 'ERROR';
            $resultado["message"] = $e->getMessage();

            return $resultado;
        }

        $now = new DateTimeImmutable();
        if ($token->iss !== $this->servername ||
            $token->iat > $now->getTimestamp() ||
            $token->exp < $now->getTimestamp())
        {
            $resultado["header"] = 'HTTP/1.1 401 Unauthorized';
            $resultado["status"] = 'ERROR';
            $resultado["message"] = 'Token expirado';

            return $resultado;
        }

        $resultado["header"] = 'HTTP/1.1 200 OK';
        $resultado["status"] = 'OK';
        $resultado["message"] = '';

        return $resultado;
    }

    public function validateToken($jwt){
        $resultado = array("header" => "", "status" => "", "message" => "");

        try {
            $this->logger->info("jwt",array("token" => $jwt));
            $token = $this->decode($jwt);        
        } 
        catch (\Exception $e) {
            $resultado["header"] = 'HTTP/1.1 401 Unauthorized';
            $resultado["status"] = 'ERROR';
            $resultado["message"] = $e->getMessage();

            return $resultado;
        }

        $now = new DateTimeImmutable();
        if ($token->iss !== $this->servername ||
            $token->iat > $now->getTimestamp() ||
            $token->exp < $now->getTimestamp())
        {
            $resultado["header"] = 'HTTP/1.1 401 Unauthorized';
            $resultado["status"] = 'ERROR';
            $resultado["message"] = 'Token expirado';

            return $resultado;
        }

        $resultado["header"] = 'HTTP/1.1 200 OK';
        $resultado["status"] = 'OK';
        $resultado["message"] = '';
        $resultado["username"] = $token->username;

        return $resultado;
    }
}