<?php
declare(strict_types=1);

namespace App\Negocio;

class Response{

    public function __construct(){ }

    public static function response(string $p_status,array $p_errores, array $p_message, array $p_data): array{
        return array("status" => $p_status, "errores" => $p_errores, "message" => $p_message, "data" => $p_data);
    }
}