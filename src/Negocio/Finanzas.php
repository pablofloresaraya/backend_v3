<?php
declare(strict_types=1);

namespace App\Negocio;

use App\Datos\Finanzas\FinanzasSql;
use Psr\Log\LoggerInterface;

class Finanzas{
    protected $company_id;
    protected $logger;

    public function __construct(LoggerInterface $logger){
        $this->logger = $logger;
    }

    public function buscarPagaresNoNulos(array $p_request): array{       
        $errores = array();
        $mensajes = array();
        $data = array();

        $opcion = $p_request["var1"];
        $fecdesde = $p_request["var2"];
        $fechasta = $p_request["var3"];
        $mes = $p_request["var4"];
        $anho = $p_request["var5"];

        if(empty($opcion)){
            $errores[] = "La opción seleccionada no es válida.";
        }
        else{
            if($opcion=="FECHA"){
                if(empty($fecdesde)){
                    $errores[] = "La fecha de inicio no es válida.";
                }
                if(empty($fechasta)){
                    $errores[] = "La fecha de término no es válida.";
                }
            }
            else if($opcion=="PERIODO"){                
                if(empty($anho)){
                    $errores[] = "El año no es válido.";
                }               
                if(empty($mes)){
                    $errores[] = "El mes no es válido.";
                }
            }
            else{
                $errores[] = "La opción seleccionada no es válida.";
            }
        }
        
        if(count($errores)==0){
            $finanzas = new FinanzasSql($this->logger);
            $clinica = new Clinica($this->logger);

            if($opcion=='FECHA'){
                $pagares = $finanzas->buscarPagaresNoNulosForFecha($fecdesde,$fechasta);
            }
            else{
                $pagares = $finanzas->buscarPagaresNoNulosForPeriodo($mes,$anho);
            }
            
            if(count($pagares)==0){
                $data["existe"] = false;
                $errores[] = "No se encontraron datos.";
            }
            else{
                $data["existe"] = true;
                //$this->logger->info("buscarPagaresNoNulos pagares", $pagares);
                for($i=0;$i<count($pagares);$i++){
                    //buscar pacientes
                    $pacientes = array();
                    $presupuestos = array();
                    $detalle = $finanzas->getPagareDetalle($pagares[$i]["ipg_cod"]);
                    $cuotas = $finanzas->getPagareCuotas($pagares[$i]["ipg_cod"]);
                    $pagares[$i]["detalle_cuotas"] = $cuotas;
                    //$this->logger->info("buscarPagaresNoNulos pagare ", $pagares[$i]);
                    //$this->logger->info("buscarPagaresNoNulos pagare detalle ", $detalle);
                    foreach ($detalle as $row) {  
                        $ppto_response = $clinica->getPresupuesto(array("var1" => $row["ps_cod"]));
                        $ppto_data = $ppto_response["data"];
                        $ppto_status = $ppto_response["status"];
                        if(count($ppto_data)>0 && $ppto_status=="ok"){
                            if(!in_array($ppto_data["pc_cod"],$pacientes)){
                                $pacientes[] = $ppto_data["pc_cod"];
                            }                                            
                            if(!in_array($ppto_data["ps_cod"],$presupuestos)){
                                $presupuestos[] = $ppto_data["ps_cod"];
                            }
                        }
                    }
                    $pc_nombres = array();
                    foreach($pacientes as $pc_cod){
                        $pc_response = $clinica->getPaciente(array("var1" => $pc_cod));
                        $pc_data = $pc_response["data"];
                        $pc_status = $pc_response["status"];
                        if(count($pc_data)>0 && $pc_status=="ok"){
                            $pc_nombres[] = $pc_data["pc_nombre"]." ".$pc_data["pc_apellido"];
                        }
                    }
                    $pagares[$i]["pacientes"] = implode(", ", $pc_nombres);
                    $pagares[$i]["presupuestos"] = implode(", ", $presupuestos);
                }
                $data["pagares"] = $pagares;
            }
        }


        if(count($errores)>0) $estado = "error";
        else $estado = "ok";

        return Response::response($estado,$errores,$mensajes,$data);
    }

    public function getPagareCuotas(array $p_request): array{       
        $errores = array();
        $mensajes = array();
        $data = array();

        $ipg_cod = $p_request["var1"];

        if(empty($ipg_cod)){
            $errores[] = "Debe seleccionar un pagaré.";
        }
        if(count($errores)==0){
            $finanzas = new FinanzasSql($this->logger);
            $cuotas = $finanzas->getPagareCuotas($ipg_cod);
            if(count($cuotas)==0){
                $data["existe"] = false;
                $errores[] = "No se encontraron datos.";
            }
            else{
                $data["existe"] = true;
                $data["cuotas"] = $cuotas;
            }
        }
        if(count($errores)>0) $estado = "error";
        else $estado = "ok";

        return Response::response($estado,$errores,$mensajes,$data);
    }

    public function buscarConveniosNoNulos(array $p_request): array{       
        $errores = array();
        $mensajes = array();
        $data = array();

        $opcion = $p_request["var1"];
        $fecdesde = $p_request["var2"];
        $fechasta = $p_request["var3"];
        $mes = $p_request["var4"];
        $anho = $p_request["var5"];

        if(empty($opcion)){
            $errores[] = "La opción seleccionada no es válida.";
        }
        else{
            if($opcion=="FECHA"){
                if(empty($fecdesde)){
                    $errores[] = "La fecha de inicio no es válida.";
                }
                if(empty($fechasta)){
                    $errores[] = "La fecha de término no es válida.";
                }
            }
            else if($opcion=="PERIODO"){                
                if(empty($anho)){
                    $errores[] = "El año no es válido.";
                }               
                if(empty($mes)){
                    $errores[] = "El mes no es válido.";
                }
            }
            else{
                $errores[] = "La opción seleccionada no es válida.";
            }
        }
        
        if(count($errores)==0){
            $finanzas = new FinanzasSql($this->logger);
            $clinica = new Clinica($this->logger);

            if($opcion=='FECHA'){
                $convenios = $finanzas->buscarConveniosNoNulosForFecha($fecdesde,$fechasta);
            }
            else{
                $convenios = $finanzas->buscarConveniosNoNulosForPeriodo($mes,$anho);
            }
            
            if(count($convenios)==0){
                $data["existe"] = false;
                $errores[] = "No se encontraron datos.";
            }
            else{
                $data["existe"] = true;
                //$this->logger->info("buscarPagaresNoNulos convenios", $convenios);
                for($i=0;$i<count($convenios);$i++){
                    //buscar pacientes
                    $pacientes = array();
                    $presupuestos = array();
                    $oexternas = array();
                    $odentales = array();
                    $detalle = $finanzas->getConvenioDetalle($convenios[$i]["ic_cod"]);
                    $cuotas = $finanzas->getConvenioCuotas($convenios[$i]["ic_cod"],$convenios[$i]["ic_oatencion"],$convenios[$i]["ic_cuotas"]);
                    $convenios[$i]["detalle_cuotas"] = $cuotas;
                    //$this->logger->info("buscarPagaresNoNulos pagare ", $convenios[$i]);
                    //$this->logger->info("buscarPagaresNoNulos pagare detalle ", $detalle);
                    foreach ($detalle as $row) {  
                        if($row["ep_tipo"]==1){
                            $orden_response = $clinica->getPresupuesto(array("var1" => $row["ps_cod"]));
                        }
                        else if($row["ep_tipo"]==2){
                            $orden_response = $clinica->getOExterna(array("var1" => $row["ps_cod"])); //Orden de Consulta
                        }
                        else if($row["ep_tipo"]==3){
                            $orden_response = $clinica->getODental(array("var1" => $row["ps_cod"])); //Orden Dental
                        }
                        $orden_data = $orden_response["data"];
                        $orden_status = $orden_response["status"];
                        if(count($orden_data)>0 && $orden_status=="ok"){
                            if(!in_array($orden_data["pc_cod"],$pacientes)){
                                $pacientes[] = $orden_data["pc_cod"];
                            }                                            
                            if($row["ep_tipo"]==1){
                                if(!in_array($orden_data["ps_cod"],$presupuestos)){
                                    $presupuestos[] = $orden_data["ps_cod"];
                                }                         
                            }  
                            else if($row["ep_tipo"]==2){
                                if(!in_array($orden_data["oe_cod"],$oexternas) && $row["ep_tipo"]==2){
                                    $oexternas[] = $orden_data["oe_cod"];
                                }      
                            }
                            else if($row["ep_tipo"]==3){
                                if(!in_array($orden_data["od_cod"],$odentales) && $row["ep_tipo"]==3){
                                    $odentales[] = $orden_data["od_cod"];
                                }
                            }
                        }
                    }
                    $pc_nombres = array();
                    foreach($pacientes as $pc_cod){
                        $pc_response = $clinica->getPaciente(array("var1" => $pc_cod));
                        $pc_data = $pc_response["data"];
                        $pc_status = $pc_response["status"];
                        if(count($pc_data)>0 && $pc_status=="ok"){
                            $pc_nombres[] = $pc_data["pc_nombre"]." ".$pc_data["pc_apellido"];
                        }
                    }
                    $convenios[$i]["pacientes"] = implode(", ", $pc_nombres);
                    $convenios[$i]["presupuestos"] = implode(", ", $presupuestos);
                    $convenios[$i]["oexternas"] = implode(", ", $oexternas);
                    $convenios[$i]["odentales"] = implode(", ", $odentales);
                }
                $data["convenios"] = $convenios;
            }
        }


        if(count($errores)>0) $estado = "error";
        else $estado = "ok";

        return Response::response($estado,$errores,$mensajes,$data);
    }
}